<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/report.class.php');

// Klassen instanziieren
$report = new report($db);

if(!empty($_POST['status']) && !empty($_POST['year']))
{
	echo json_encode($report->reportList($_POST['status'], $_POST['year'], $_POST['searchedVal']));
}

if(empty($_POST['status']))
{
	http_response_code(404);
    echo "Es wurde kein Status ausgewählt!";
}

if(empty($_POST['year']))
{
	http_response_code(404);
    echo "Es wurde kein Jahr ausgewählt!";
}