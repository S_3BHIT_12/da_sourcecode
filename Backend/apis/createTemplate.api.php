<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/template.class.php');

// Klassen instanziieren
$template = new template($db);

$templateid = $template->createTemplate($_POST['imgcount'], $_POST['htmlcode'], $_POST['latexcode']);
$filename = "template".$templateid["LAST_INSERT_ID()"].".tex";	//directory templates with filename template40.tex 40..id
//.tex Datei erstellen

	$myfile = fopen("../templates/".$filename, "w") or die("Unable to open file!"); //create or open file
	$txt = $_POST['latexcode'];
	fwrite($myfile, $txt); //write code in it
	fclose($myfile); //close file
	
	chdir(dirname(realpath("../templates/".$filename))); //voller Pfad --> nicht mehr auf File sonst auf dessen Ordner
	$console = shell_exec("pdflatex {$filename}"); //execute latex on it