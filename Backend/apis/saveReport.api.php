<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/report.class.php');

// Klassen instanziieren
$report = new report($db);

//if empty
if(!empty($_POST['template']) && !empty($_POST['reportid']))
{
	$tempid = intval($_POST['template']);
	echo $report->reportUpdate($_POST['reportid'],$_POST['heading'],$_POST['content'],$tempid);
}

if(empty($_POST['template']))
{
	http_response_code(404);
    echo "Es wurde kein Template ausgewählt!";
}

if(empty($_POST['reportid']))
{
	http_response_code(404);
    echo "Es wurde kein Bericht ausgewählt!";
}