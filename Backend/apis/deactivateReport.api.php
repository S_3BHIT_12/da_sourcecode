<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/user.class.php');
include('../classes/report.class.php');

// Klassen instanziieren
$user = new user($db);
$report = new report($db);

//if empty
if(!empty($_GET['id']))
{
	$report->deactivateReport($_GET['id'], "report");
	header("location: ../pages/report.php");
}
else{
	http_response_code(422);
    echo "Invalid parameters given!";
}