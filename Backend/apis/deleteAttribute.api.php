<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/report.class.php');

// Klassen instanziieren
$report = new report($db);

if(!empty($_POST['id']) && $_POST['reportid'])
{
	echo json_encode($report->deleteAttribute($_POST['id'], $_POST['reportid']));
}

else
{
	http_response_code(404);
    echo "Kein Attribut ausgewählt!";
}