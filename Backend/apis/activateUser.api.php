<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/user.class.php');
include('../classes/report.class.php');

// Klassen instanziieren
$user = new user($db);
$report = new report($db);
//if empty
if(!empty($_GET['id']))
{
	$user->activateUser($_GET['id']);
	$report->activateReport($_GET['id'], "user");
	header("location: ../pages/user.php");
}
else{
	http_response_code(422);
    echo "Invalid parameters given!";
}