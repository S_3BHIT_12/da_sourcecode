<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/template.class.php');
include('../classes/report.class.php');

// Klassen instanziieren
$template = new template($db);
$report = new report($db);
//if empty
if(!empty($_GET['id']))
{
	$template->deactivateTemplate($_GET['id']);
	$report->deactivateReport($_GET['id'], "template");
	header("location: ../pages/template.php");
}
else{
	http_response_code(422);
    echo "Invalid parameters given!";
}