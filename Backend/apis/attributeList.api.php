<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/report.class.php');

// Klassen instanziieren
$report = new report($db);

if(!empty($_POST['id']))
{
	echo json_encode($report->attributeList($_POST['id']));
}

else
{
	http_response_code(404);
    echo "Es wurde kein Bericht ausgewählt!";
}