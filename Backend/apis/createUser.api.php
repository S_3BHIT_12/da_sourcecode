<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/user.class.php');
include('../classes/permissiongroup.class.php');

// Klassen instanziieren
$user = new user($db);
$permissiongroup = new permissiongroup($db);

//if empty
if(!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['email']) && !empty($_POST['phonenumber']) && !empty($_POST['permission']))
{
	$user->createUser($_POST['username'],$_POST['password'],$_POST['email'],$_POST['phonenumber'],$_POST['permission']);
	$permissiongroup->setPermission($_POST['username'],$_POST['permission']);
}
else
{
	http_response_code(404);
    echo "Alle Werte bitte ausfüllen!";
}