<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/image.class.php');

// Klassen instanziieren
$image = new image($db);

if(!empty($_GET['id']))
{
	$image->deleteimg($_GET['id']);
	header("location: ../pages/editReport.php");
}
else{
	http_response_code(422);
    echo "Invalid parameters given!";
}