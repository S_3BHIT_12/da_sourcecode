<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/template.class.php');

// Klassen instanziieren
$template = new template($db);

//if empty
if(!empty($_POST['id']))
{
	$template->templateUpdate($_POST['id'],$_POST['htmlcode'],$_POST['latexcode'], $_POST['imgcount']);
	
	$filename = "template".$_POST['id'];	//directory templates with filename template40.tex 40..id
	//.tex Datei erstellen

	$myfile = fopen("../templates/".$filename.".tex", "w") or die("Unable to open file!"); //create or open file
	$txt = $_POST['latexcode'];
	fwrite($myfile, ""); //Löschen von Inhalt von File
	fwrite($myfile, $txt); //write code in it
	fclose($myfile); //close file
	
	chdir(dirname(realpath("../templates/".$filename.".tex"))); //voller Pfad --> nicht mehr auf File sonst auf dessen Ordner
	$console = shell_exec("pdflatex {$filename}"); //execute latex on it*/
	
}
else
{
	http_response_code(404);
    echo "Es wurde kein Template ausgewählt!";
}