<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/user.class.php');

// Klassen instanziieren
$user = new user($db);

//if empty
if(!empty($_POST['id']) && !empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['email']) && !empty($_POST['phonenumber']))
{
	echo $user->userUpdate(intval($_POST['id']),$_POST['username'],$_POST['password'],$_POST['email'],$_POST['phonenumber']);
}
else
{
	http_response_code(404);
    echo "Alle Werte bitte ausfüllen!";
}