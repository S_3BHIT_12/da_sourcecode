<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/user.class.php');
include('../classes/instrument.class.php');

// Klassen instanziieren
$user = new user($db);
$instrument = new instrument($db);

// Parameter prüfen - wenn vorhanden
if(isset($_GET["param"]) && !empty($_POST["param"])) {
    // Logik ausführen, requests an Klassen absetzen
    $user->methodenName($param1, $param2);
}
else { // Verhalten bei ungültiger Parameterübergabe
    http_response_code(422);
    echo "Invalid parameters given!";
}