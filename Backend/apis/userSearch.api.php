<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/user.class.php');

// Klassen instanziieren
$user = new user($db);

//if empty
if(!empty($_POST['searchedVal']))
{
	echo json_encode($user->searchUser($_POST['searchedVal']));
}

else { //bei keiner Eingabe --> ganze Liste anzeigen
	echo json_encode($user->userList());
}