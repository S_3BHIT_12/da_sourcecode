<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/report.class.php');

// Klassen instanziieren
$report = new report($db);

if(!empty($_POST['reportid']) && !empty($_POST['name']) && !empty($_POST['value']))
{
	echo json_encode($report->createAttribute($_POST['reportid'],$_POST['name'],$_POST['value']));
}

else
{
	http_response_code(404);
    echo "Alle Werte ausfüllen!";
}