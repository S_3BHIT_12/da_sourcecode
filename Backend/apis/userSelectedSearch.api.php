<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/user.class.php');

// Klassen instanziieren
$user = new user($db);

if(!empty($_POST['id']))
{
	echo json_encode($user->searchSelectedUser($_POST['id']));
}

else
{
	http_response_code(404);
    echo "Es wurde kein User ausgewählt!";
}