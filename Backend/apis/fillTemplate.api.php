<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/template.class.php');
include('../classes/report.class.php');
include('../classes/image.class.php');

// Klassen instanziieren
$template = new template($db);
$report = new report($db);
$image = new image($db);

if(!empty($_POST['template']) && !empty($_POST['report']))
{
	$selectedtemplate = $template->templateLoad($_POST['template']);
	$attributes = $report->attributeList($_POST['report']);
	$images = $image->getimgage($_POST['report']);
	
	if($selectedtemplate[0]['imgcount'] <= count($images)){

		$buildtable = '';
		for ($i = 0; $i < count($attributes); $i++) {
			$buildtable = $buildtable . '<tr><td>'.$attributes[$i]['name'].'</td><td>'.$attributes[$i]['value'].'</td></tr>';
		}
		
		for($i = 0; $i < count($images); $i++){
			$img = 'src="data:image/gif;base64,'.$images[$i]['code'].'"';

			$image = 'image'.$i;
			$smarty->assign($image, $img);
		}
		
		$smarty->assign('table', $buildtable);
		$smarty->assign('u1', $_POST['heading']);
		$smarty->assign('content', $_POST['content']);
		$html_content = $selectedtemplate[0]['html_content'];
		$smarty->display('string:'.$html_content);
	}
	else
		echo "<strong>Achtung!</strong>Es befinden sich zu wenig Fotos im Bericht, um dieses Template auszuwählen!!";
}

if(empty($_POST['template']))
{
	http_response_code(404);
    echo "Es wurde kein Template ausgewählt!";
}

if(empty($_POST['report']))
{
	http_response_code(404);
    echo "Es wurde kein Bericht ausgewählt!";
}