<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/report.class.php');

// Klassen instanziieren
$report = new report($db);

if(!empty($_POST['year']))
{
	
	echo json_encode($report->reportYearList($_POST['year']));
	//$attributeList = $report->allAttributes();
}

if(empty($_POST['year']))
{
	http_response_code(404);
    echo "Es wurde kein Jahr ausgewählt!";
}