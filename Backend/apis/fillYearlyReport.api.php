<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/template.class.php');
include('../classes/report.class.php');
include('../classes/image.class.php');
include('../classes/user.class.php');
// Klassen instanziieren
$template = new template($db);
$report = new report($db);
$image = new image($db);
$user = new user($db);

$yearlyHTMLReport = '';
$yearlyLATEXReport = '\documentclass[12pt,twoside,a4paper]{article}
\usepackage{tabularx}
\usepackage[a4paper, total={16cm, 29cm}]{geometry}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}

\makeatletter
% we use \prefix@<level> only if it is defined
\renewcommand{\@seccntformat}[1]{%
  \ifcsname prefix@#1\endcsname
    \csname prefix@#1\endcsname
  \else
    \csname the#1\endcsname\quad
  \fi}
% define \prefix@section
\newcommand\prefix@section{}
\makeatother
\begin{document}
';
if(!empty($_GET['reportList']))
{
	$reportList = json_decode($_GET['reportList'], true);
	//HTML DATEI AUFBAU
	for($i = 0; $i < count($reportList); $i++)
	{
		$selectedtemplate = $template->templateLoad($reportList[$i]['template_id']);
	
		$attributes = $report->attributeList($reportList[$i]['id']);
		$buildtable = '';
		for ($j = 0; $j < count($attributes); $j++) {
			$buildtable = $buildtable . '<tr><td>'.$attributes[$j]['name'].'</td><td>'.$attributes[$j]['value'].'</td></tr>';
		}
		
		$images = $image->getimgage($reportList[$i]['id']);
		for($k = 0; $k < count($images); $k++){
			$img = 'src="data:image/gif;base64,'.$images[$k]['code'].'"';

			$image = 'image'.$k;
			$smarty->assign($image, $img);
		}
		
		$smarty->assign('table', $buildtable);
		$smarty->assign('u1', $reportList[$i]['heading']);
		$smarty->assign('content', $reportList[$i]['content']);
	
		$html_content = $selectedtemplate[0]['html_content'];

		$yearlyHTMLReport .= '<div style="width:21cm; height:29.7cm; border: 1px solid black; margin:0 auto; margin-bottom: 20px;"id="'.$reportList[$i]['id'].'">' . $smarty->fetch('string:'.$html_content) . '</div>'; //fetch für String return
	} 
	
	$filename = "jahresbericht_".$_GET['year'];	//directory templates with filename template40.tex 40..id
	//.tex Datei erstellen

	$myfile = fopen("../jahresbericht/".$filename.".html", "w"); //create or open file
	fwrite($myfile, ""); //Löschen von Inhalt von File
	fwrite($myfile, $yearlyHTMLReport); //write code in it
	fclose($myfile); //close file
	
	$smarty->left_delimiter = '<@{';
	$smarty->right_delimiter = '}@>';
	for($i = 0; $i < count($reportList); $i++)
	{
		$selectedtemplate = $template->templateLoad($reportList[$i]['template_id']);
	
		$attributes = $report->attributeList($reportList[$i]['id']);
		$buildtable = '\hline ';
		for ($j = 0; $j < count($attributes); $j++) {
			$buildtable = $buildtable . $attributes[$j]['name'].' & '.$attributes[$j]['value'].' \\\ \hline ';
		}
		
		$username = $user->getusername($reportList[$i]['id']);
	
		$reportid = $reportList[$i]['id'];
		$year = $report->getyear($reportList[$i]['id']);
		for($k = 0; $k < count($images); $k++){
			$getimgfile = 'C:/xampp/htdocs/da_sourcecode/Frontend/uploads/'.$username[0]['username'].'/'.$year[0]['year'].'/'.$reportid."-".$k.'.png';
			$image = 'image'.$k;
			$smarty->assign($image, $getimgfile);

		}
		
		$smarty->assign('table', $buildtable);
		$smarty->assign('u1', $reportList[$i]['heading']);
		$smarty->assign('content', $reportList[$i]['content']);
	
		$latex_content = $selectedtemplate[0]['latex_content'];
		$yearlyLATEXReport .= $smarty->fetch('string:'.$latex_content).'\newpage'; //fetch für String return
	}
	
	$yearlyLATEXReport .= '\end{document}';
	//var_dump($yearlyLATEXReport);

	$myfile = fopen("../jahresbericht/".$filename.".tex", "w");//create or open file
	fwrite($myfile, ""); //Löschen von Inhalt von File
	fwrite($myfile, $yearlyLATEXReport); //write code in it
	fclose($myfile); //close file
	
	chdir(dirname(realpath("../jahresbericht/".$filename.".tex"))); //voller Pfad --> nicht mehr auf File sonst auf dessen Ordner
	$console = shell_exec("pdflatex {$filename}"); //execute latex on it

	$files = array($filename.'.html', $filename.'.pdf');
	$zipname = $filename.'.zip';
	$zip = new ZipArchive;
	$zip->open("../jahresbericht/".$zipname, ZipArchive::CREATE);
	foreach ($files as $file) {
		$zip->addFile($file);
	}
	$zip->close();
	
	
	//Download mit Header html
	header("Content-disposition: attachment; filename=" . $zipname);
	header("Content-type: application/zip");
	readfile("../jahresbericht/".$zipname);
	
	//header('location: ../pages/preview.php?year='.$_GET['year']);
}

if(empty($_GET['reportList']))
{
	http_response_code(404);
    echo "Es wurde kein Bericht ausgewählt!";
}