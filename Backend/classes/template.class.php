<?php
class template
{
	private $db;
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function countTemplate()
	{
		$stmt = $this->db->query('SELECT id FROM template;');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function templateList()
	{
		$stmt = $this->db->query('SELECT * FROM template;');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function createTemplate($imgcount, $htmlcode, $latexcode)
	{
	    $stmt = $this->db->prepare('INSERT INTO template (html_content, latex_content, deleted, imgcount) VALUES (:htmlcode, :latexcode,FALSE, :imgcount)');
		$stmt->bindParam(':htmlcode', $htmlcode, PDO::PARAM_STR);
		$stmt->bindParam(':latexcode', $latexcode, PDO::PARAM_STR);
		$stmt->bindParam(':imgcount', $imgcount, PDO::PARAM_STR);
		$stmt->execute();
		$stmt = $this->db->query('SELECT LAST_INSERT_ID() from template'); //for tex File name
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}
	
	public function deactivateTemplate($id)
    {
        $stmt = $this->db->prepare('UPDATE template SET deleted=TRUE where id=:id');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
    }
	
	public function activateTemplate($id)
    {
        $stmt = $this->db->prepare('UPDATE template SET deleted=FALSE where id=:id');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
    }
	
	public function templateLoad($id)
	{
		$stmt = $this->db->prepare('SELECT * from template where id=:id');
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function templateUpdate($id, $htmlcode, $latexcode, $imgcount)
    {
		$stmt = $this->db->prepare('UPDATE template SET html_content=:html_content, latex_content=:latex_content, imgcount=:imgcount WHERE id=:id');
		$stmt->bindParam(':html_content', $htmlcode, PDO::PARAM_STR);
		$stmt->bindParam(':latex_content', $latexcode, PDO::PARAM_STR);
		$stmt->bindParam(':imgcount', $imgcount, PDO::PARAM_STR);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
        //$stmt = $this->db->query('UPDATE template SET html_content="'.$htmlcode.'", latex_content="'.$latexcode.'" WHERE id='.$id);
        return $stmt;
    }
}