<?php
class report
{
	private $db;
	
	public function __construct($db)
	{
		$this->db = $db;
	}
		
	public function reportCount()
	{
		$stmt = $this->db->query('SELECT id FROM report;');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function yearList()
	{
		$stmt = $this->db->query('SELECT distinct year from report;');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function attributeList($id)
	{
		$stmt = $this->db->query('SELECT * from table_row where report_id = '.$id);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function deleteAttribute($id, $reportid)
	{
		$stmt = $this->db->query('delete from table_row where id = '.$id);
		return $this->attributeList($reportid);
	}
	
	public function createAttribute($reportid, $name, $value)
	{
		$stmt = $this->db->prepare('insert into table_row (name, report_id, value) values (:name, :reportid, :value)');
		$stmt->bindParam(':name', $name, PDO::PARAM_STR);
		$stmt->bindParam(':reportid', $reportid);
		$stmt->bindParam(':value', $value);
		$stmt->execute();
		return $this->attributeList($reportid);
	}
	
	public function reportYearList($year)
	{	
		$stmt = $this->db->query('select r.id, r.template_id, r.heading, r.content from report r, status s where r.year='.$year.' and s.accepted=TRUE and s.report_id=r.id');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	
	public function reportList($status, $year, $searchedVal)
	{
		$searchedVal =  '%'.$searchedVal.'%';
		if($status == 1)
		{
			$stmt = $this->db->prepare('SELECT r.id, u.username, r.year, s.accepted FROM report r, status s, user u where r.id = s.report_id and u.id = r.user_id and r.year = '.$year.' and s.accepted=TRUE and u.username LIKE :searchedVal and u.id != (SELECT user_id from user_has_permissiongroup where permissiongroup_id = 2);');
			$stmt->bindParam(':searchedVal', $searchedVal, PDO::PARAM_STR);
			$stmt->execute();
		}
		if($status == 2)
		{
			$stmt = $this->db->prepare('SELECT r.id, u.username, r.year, s.submitted FROM report r, status s, user u where r.id = s.report_id and u.id = r.user_id and r.year = '.$year.' and s.submitted=TRUE and u.username LIKE :searchedVal and u.id != (SELECT user_id from user_has_permissiongroup where permissiongroup_id = 2);');
			$stmt->bindParam(':searchedVal', $searchedVal, PDO::PARAM_STR);
			$stmt->execute();
		}
		
		if($status == 3)
		{
			$stmt = $this->db->prepare('SELECT r.id, u.username, r.year, s.declined FROM report r, status s, user u where r.id = s.report_id and u.id = r.user_id and r.year = '.$year.' and s.declined=TRUE and u.username LIKE :searchedVal and u.id != (SELECT user_id from user_has_permissiongroup where permissiongroup_id = 2);');
			$stmt->bindParam(':searchedVal', $searchedVal, PDO::PARAM_STR);
			$stmt->execute();
		}
		
		if($status == 4)
		{
			$stmt = $this->db->prepare('SELECT r.id, u.username, r.year, s.deleted FROM report r, status s, user u where r.id = s.report_id and u.id = r.user_id and r.year = '.$year.' and s.deleted=TRUE and u.username LIKE :searchedVal and u.id != (SELECT user_id from user_has_permissiongroup where permissiongroup_id = 2);');
			$stmt->bindParam(':searchedVal', $searchedVal, PDO::PARAM_STR);
			$stmt->execute();
		}
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function reportLoad($id)
	{
		$stmt = $this->db->query('SELECT r.heading, r.content, t.html_content, t.latex_content, t.id FROM report r, template t where r.id = '.$id.' and r.template_id = t.id;');
		
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function deactivateReport($id, $where)
    {
		if($where == "user")
		{
			$stmt = $this->db->query('SELECT id from report where user_id = '.$id);
			$reports = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
			for($i = 0; $i < count($reports); $i++){
				$stmt = $this->db->query('UPDATE status set accepted=FALSE, submitted=FALSE, declined=FALSE, deleted=TRUE where report_id='. $reports[$i]['id']);
			}
		}
		if($where == "template")
		{
			$stmt = $this->db->query('SELECT id FROM report where template_id = '. $id);
			$reports = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			for($i = 0; $i < count($reports); $i++)
			{
				$stmt = $this->db->query('UPDATE report set template_id = 1 where report_id='. reports[$i]['id']);
			}
		}
		if($where == "report")
		{	
			$stmt = $this->db->query('UPDATE status set accepted=FALSE, submitted=FALSE, declined=FALSE, deleted=TRUE where report_id='. $id);
		}
        return $stmt;
    }
	
	public function activateReport($id, $where)
    {
		if(where == "user")
		{
			$stmt = $this->db->query('SELECT id from report where user_id = '.$id);
			$reports = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
			for($i = 0; $i < count($reports); $i++){
				$stmt = $this->db->query('UPDATE status set accepted=FALSE, submitted=TRUE, declined=FALSE, deleted=FALSE where report_id='.$reports[$i]['id']);
			}
		}
		if($where == "report")
		{	
			$stmt = $this->db->prepare('UPDATE status set accepted=FALSE, submitted=TRUE, declined=FALSE, deleted=FALSE where report_id=:id');
			$stmt->bindParam(':id', $id, PDO::PARAM_INT);
			$stmt->execute();
		}
        return $stmt;
    }
	public function acceptReport($id)
    {	
		$stmt = $this->db->prepare('UPDATE status set accepted=TRUE, submitted=FALSE, declined=FALSE, deleted=FALSE where report_id=:id');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
    }
	public function declineReport($id)
    {	
		$stmt = $this->db->prepare('UPDATE status set accepted=FALSE, submitted=FALSE, declined=TRUE, deleted=FALSE where report_id=:id');
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
    }
	
	public function reportUpdate($id, $heading, $content, $template)
    {
		$stmt = $this->db->prepare('UPDATE report SET heading=:heading, content=:content, template_id=:template_id WHERE id=:id');
		$stmt->bindParam(':heading', $heading, PDO::PARAM_STR);
		$stmt->bindParam(':content', $content, PDO::PARAM_STR);
		$stmt->bindParam(':template_id', $template, PDO::PARAM_INT);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
    }
	
	public function getyear($id)
	{
		$stmt = $this->db->prepare('SELECT * from report where id =:reportid');
		$stmt->bindParam(':reportid', $id, PDO::PARAM_STR);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
}