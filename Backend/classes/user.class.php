<?php
class user
{
	private $db;
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function countUser()
	{
		$stmt = $this->db->query('SELECT id FROM user;');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function userList()
	{
		$stmt = $this->db->query('SELECT * FROM user;');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function searchUser($searchedVal)
	{
		$searchedVal =  '%'.$searchedVal.'%';
		$stmt = $this->db->prepare('SELECT * FROM user where username LIKE :searchedVal');
		$stmt->bindParam(':searchedVal', $searchedVal, PDO::PARAM_STR);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function searchSelectedUser($id)
	{
		$stmt = $this->db->prepare('SELECT * from user where id=:id');
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function userUpdate($id, $username, $password, $email, $phonenumber)
    {
        $stmt = $this->db->prepare('UPDATE user SET username=:username, password=:password, email=:email, phonenumber=:phonenumber WHERE id=:id');
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt->bindParam(':password', $password, PDO::PARAM_STR);
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt->bindParam(':phonenumber', $phonenumber, PDO::PARAM_STR);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function deactivateUser($id)
    {
        $stmt = $this->db->prepare('UPDATE user SET deleted=TRUE where id=:id');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }
	
	public function activateUser($id)
    {
        $stmt = $this->db->prepare('UPDATE user SET deleted=FALSE where id=:id');
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }
	
	public function createUser($username,$password,$email,$phonenumber,$permission)
    {
        $stmt = $this->db->prepare('INSERT INTO user (username, password, email, phonenumber, deleted) VALUES (:username, :password, :email, :phonenumber, FALSE)');
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt->bindParam(':password', $password, PDO::PARAM_STR);
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt->bindParam(':phonenumber', $phonenumber, PDO::PARAM_STR);
		$stmt->execute();
    }
	
	public function getusername($id)
	{
		$stmt = $this->db->prepare('SELECT * FROM report WHERE id=:reportid');
		$stmt->bindParam(':reportid', $id, PDO::PARAM_STR);
		$stmt->execute();
		//$stmt->fetchAll(PDO::FETCH_ASSOC);
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$userid = $row['user_id'];
		}
		
		var_dump($userid);
		$username = $this->db->prepare('SELECT * FROM user WHERE id=:userid');
		$username->bindParam(':userid', $userid, PDO::PARAM_STR);
		$username->execute();
		return $username->fetchAll(PDO::FETCH_ASSOC);
	}
}