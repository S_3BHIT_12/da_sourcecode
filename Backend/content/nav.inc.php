</head>
<body>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div  style="">
            <a class="navbar-brand" href="index.php">Jahresbericht-Admin</a>	
        </div> <!--INDEX LINK-->
		<div style="float:right;">
			<a class="navbar-brand" href="../apis/logout.api.php">Abmelden</a> <!--LOGOUT-->
		</div>
	</nav>
        <div class="sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="../pages/user.php">
							<p style="margin-bottom: 0px; display:inline;">Userliste</p>
							<p id="userCount" style="margin-bottom: 0px; display:inline; color:red;"></p>
						</a>
                    </li>
                    <li>
                        <a href="../pages/template.php">
							<p style="margin-bottom: 0px; display:inline;">Templates</p>
							<p id="templateCount" style="margin-bottom: 0px; display:inline; color:red;"></p>
						</a>
                    </li>
					<li>
					    <a href="report.php">
							<p style="margin-bottom: 0px; display:inline;">Berichte</p>
							<p id="reportCount" style="margin-bottom: 0px; display:inline; color:red;"></p>
						</a>
                    </li>
					<li>
                        <a href="preview.php">Jahresbericht</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
<script>
	//document is loaded
    $(document).ready(function() {
        $.ajax({
            url:"../apis/userCount.api.php",
            success:function(data) {
				//console.log('UserCount works!');
                var userCount = JSON.parse(data);
				//console.log(userCount.length);
				//console.log(data);
				$('#userCount').append(userCount.length);
				
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
	
		$.ajax({
            url:"../apis/templateCount.api.php",
            success:function(data) {
				//console.log('TemplateCount works!');
                var templateCount = JSON.parse(data);
				//console.log(templateCount.length);
				//console.log(data);
				$('#templateCount').append(templateCount.length);
				
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
		
		$.ajax({
            url:"../apis/reportCount.api.php",
            success:function(data) {
				//console.log('reportCount works!');
                var reportCount = JSON.parse(data);
				//console.log(reportCount.length);
				//console.log(data);
				$('#reportCount').append(reportCount.length);
				
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
	});
</script>