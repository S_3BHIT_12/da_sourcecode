<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Diplomarbeit >>Tut Gut!<<">
    <meta name="author" content="Pfeffer Daniel und Moser Tobias">

    <!-- Bootstrap Core CSS -->
    <link href="/da_sourcecode/Backend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	

    <!-- MetisMenu CSS -->
    <link href="/da_sourcecode/Backend/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/da_sourcecode/Backend/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/da_sourcecode/Backend/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/da_sourcecode/Backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
    <!-- jQuery -->
    <script src="/da_sourcecode/Backend/vendor/jquery/jquery.min.js"></script>
	<script src="/da_sourcecode/Backend/vendor/bootstrap/js/bootstrap.min.js"></script>