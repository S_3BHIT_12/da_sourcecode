<?php
/* Smarty version 3.1.30, created on 2017-03-19 19:34:51
  from "cf7ab968a4981743871e7833c2729fe361422896" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58cecf4b2c4640_89828765',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58cecf4b2c4640_89828765 (Smarty_Internal_Template $_smarty_tpl) {
?>
\usepackage{graphicx}

\title{<?php echo $_smarty_tpl->tpl_vars['u1']->value;?>
}
\begin{document}
\begin{tabular}{| l | l |}
<?php echo $_smarty_tpl->tpl_vars['table']->value;?>

\end{tabular}
<?php echo $_smarty_tpl->tpl_vars['content']->value;?>


\includegraphics[scale=0.5]{<?php echo $_smarty_tpl->tpl_vars['image0']->value;?>
}
\includegraphics[scale=0.4]{<?php echo $_smarty_tpl->tpl_vars['image1']->value;?>
}
<?php }
}
