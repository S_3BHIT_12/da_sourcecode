<?php
/* Smarty version 3.1.30, created on 2017-02-28 12:08:32
  from "4e4b543da19f55ee75a8e6ed174d7c907fbfa624" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58b55a303d9423_49241630',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58b55a303d9423_49241630 (Smarty_Internal_Template $_smarty_tpl) {
?>
\documentclass[12pt]{article}
%	options include 12pt or 11pt or 10pt
%	classes include article, report, book, letter, thesis
\title{Jahresbericht}
\author{Moser Tobias \\ Pfeffer Daniel}
\date{16.02.2017}
\begin{document}
\maketitle
\section{Bericht}
\begin{table}
\label{my-label}
\begin{tabular}{|l|l|}
{<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
}
\end{tabular}
\end{table}
{<?php echo $_smarty_tpl->tpl_vars['u1']->value;?>
}
{<?php echo $_smarty_tpl->tpl_vars['content']->value;?>
}
\end{document}
<?php }
}
