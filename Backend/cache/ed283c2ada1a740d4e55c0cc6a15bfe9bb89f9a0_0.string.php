<?php
/* Smarty version 3.1.30, created on 2017-03-21 13:50:46
  from "ed283c2ada1a740d4e55c0cc6a15bfe9bb89f9a0" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d121a6971869_37584509',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58d121a6971869_37584509 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
#bericht{
font-family: verdana, san-serif;
font-size:15pt;
}

#bericht table {
    font-family: verdana, sans-serif;
    border-collapse: collapse;
    width: 100%;
margin-bottom: 15px;
}

#bericht td, th {
    border: 2px solid black;
    text-align: left;
    padding: 8px;
}

img
{
border:15px solid black;
border-radius: 50px;
}

</style>

<div style="margin: 20px" id ="bericht">
<table>
<?php echo $_smarty_tpl->tpl_vars['table']->value;?>

</table>

<div style="line-height: 1.5;">
<img style=" width:6.5cm; height:6.5cm;" <?php echo $_smarty_tpl->tpl_vars['image0']->value;?>
>
<img style=" width:6.5cm; height:6.5cm;" <?php echo $_smarty_tpl->tpl_vars['image1']->value;?>
>
<img style=" width:6.5cm; height:6.5cm;" <?php echo $_smarty_tpl->tpl_vars['image2']->value;?>
>
<div>

<h1 style="font-size: 30pt; font-weight: bold;">
<?php echo $_smarty_tpl->tpl_vars['u1']->value;?>

</h1>

<p><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</p>

<div><?php }
}
