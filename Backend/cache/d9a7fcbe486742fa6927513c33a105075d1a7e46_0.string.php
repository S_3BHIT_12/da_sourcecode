<?php
/* Smarty version 3.1.30, created on 2017-03-21 17:54:15
  from "d9a7fcbe486742fa6927513c33a105075d1a7e46" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d15ab767ebc1_97644484',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58d15ab767ebc1_97644484 (Smarty_Internal_Template $_smarty_tpl) {
?>
<html>
<style>
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}
.heading{
    font-family: verdana;
    font-size: 300%;
    margin: 2%;
}
.content{
  margin:2.5%;
  font-family: arial;
  font-size: 120%;
}
.img0{
background-size: 100% auto;
 position: relative; 
width: 8cm; 
height: 8cm}
</style>
<body>
<table><?php echo $_smarty_tpl->tpl_vars['table']->value;?>
</table>
<h1 class="heading"><?php echo $_smarty_tpl->tpl_vars['u1']->value;?>
</h1>
<p class="content"><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</p>
<img class="img0" style="" <?php echo $_smarty_tpl->tpl_vars['image0']->value;?>
>
<img style="background-size: 100% auto; position: relative; width: 149px; height: 149px" <?php echo $_smarty_tpl->tpl_vars['image1']->value;?>
>
</body>
</html>
<?php }
}
