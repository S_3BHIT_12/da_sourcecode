<?php
/* Smarty version 3.1.30, created on 2017-03-19 19:29:04
  from "6803f03412a1df2588075fd982a2c7426203ed46" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58cecdf005b121_59746229',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58cecdf005b121_59746229 (Smarty_Internal_Template $_smarty_tpl) {
?>
\documentclass{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}

\title{<?php echo $_smarty_tpl->tpl_vars['u1']->value;?>
}
\begin{document}
\begin{tabular}{| l | l |}
<?php echo $_smarty_tpl->tpl_vars['table']->value;?>

\end{tabular}
<?php echo $_smarty_tpl->tpl_vars['content']->value;?>


\includegraphics[scale=0.5]{<?php echo $_smarty_tpl->tpl_vars['image0']->value;?>
}
\includegraphics[scale=0.4]{<?php echo $_smarty_tpl->tpl_vars['image1']->value;?>
}

\end{document}<?php }
}
