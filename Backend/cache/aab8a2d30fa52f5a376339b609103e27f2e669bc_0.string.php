<?php
/* Smarty version 3.1.30, created on 2017-03-19 19:35:27
  from "aab8a2d30fa52f5a376339b609103e27f2e669bc" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58cecf6f143c69_49396934',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58cecf6f143c69_49396934 (Smarty_Internal_Template $_smarty_tpl) {
?>
\usepackage{graphicx}

\title{<?php echo $_smarty_tpl->tpl_vars['u1']->value;?>
}
\begin{document}
\begin{tabular}{| l | l |}
<?php echo $_smarty_tpl->tpl_vars['table']->value;?>

\end{tabular}
<?php echo $_smarty_tpl->tpl_vars['content']->value;?>


\includegraphics[scale=0.1]{<?php echo $_smarty_tpl->tpl_vars['image0']->value;?>
}
\includegraphics[scale=0.1]{<?php echo $_smarty_tpl->tpl_vars['image1']->value;?>
}
<?php }
}
