<?php
/* Smarty version 3.1.30, created on 2017-02-10 11:20:03
  from "f3f2e7fc49f75fffd639a142101875c51f1dad8b" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_589d93d31a3867_22076569',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_589d93d31a3867_22076569 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: green;
}
</style>

<table>
  <?php echo $_smarty_tpl->tpl_vars['table']->value;?>

</table>

<h1 style="color:green"><?php echo $_smarty_tpl->tpl_vars['u1']->value;?>
</h1>
<p><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</p><?php }
}
