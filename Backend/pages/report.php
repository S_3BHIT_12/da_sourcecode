<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<div id="page-wrapper">
    <div class="row"><div class="col-lg-12">
            <h1 class="page-header">Berichtliste</h1>
        </div></div>

    <div class="row">
	
        <div class="col-lg-12" style="margin-bottom: 10px;"> <!--Search input -->
			<div class="input-group custom-search-form col-lg-12">
                <input id="searchedVal" type="text" class="form-control" placeholder="Benutzername eingeben">
            </div>
		</div> <!-- Search input-->
		
		<div class="col-lg-2" style="margin-bottom: 10px;"> 
			<div class="form-group">
                <select class="form-control" id="yearList">
				
                </select>
            </div>
		</div>
		
		<div class="col-lg-2" style="margin-bottom: 10px;"> 
			<div class="form-group">
                <select class="form-control" id="statusList">
					<option data-id="1">Akzeptiert</option>
					<option data-id="2">Eingereicht</option>
					<option data-id="3">Abgelehnt</option>
					<option data-id="4">Deaktiviert</option>
                </select>
            </div>
		</div>
		
        <div class="table-responsive col-lg-12"> <!-- -->
            <table class="table table-striped table-bordered table-hover">
                <thead> <!-- Head of Table-->
                    <tr>
                        <th>ID</th>
                        <th>Benutzername</th>
                        <th>Erstellungsjahr</th>
						<th>Funktionen</th>
                    </tr>
                </thead>
                <tbody id="tablebody"> <!-- Content of Table (document ready)-->
					<tr>
					</tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
<?php include("../content/head.inc.php"); ?>

<script>

    //document is loaded
    $(document).ready(function() {
	        $.ajax({
            url:"../apis/yearList.api.php",
            success:function(data) {

                var yearList = JSON.parse(data);
				for(i = 0; i < yearList.length; i++) {
					$('#yearList').append('<option>'+yearList[i]['year']+'</option>');
				}
				$('#yearList').val((new Date).getFullYear());
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
		
		$.ajax({
            url:"../apis/reportList.api.php",
			type: "POST",
			data: {	searchedVal: $('#searchedVal').val(),
					status: $('#statusList option:selected').attr("data-id"),
					year: (new Date).getFullYear()
			},
            success:function(data) {
				console.log(data);
                var reportList = JSON.parse(data);
				var status = $('#statusList option:selected').attr("data-id");
				console.log(reportList);
				fillTable(reportList, status);
            },
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
        });
	});
	
	$('#searchedVal').keyup(function() {
        $.ajax({
			url:'../apis/reportList.api.php',
			type:'POST',
			data: {	searchedVal: $('#searchedVal').val(),
					status: $('#statusList option:selected').attr("data-id"),
					year: $('#yearList option:selected').val()},
			
            success:function(data) {
				$('#tablebody').empty();
				var status = $('#statusList option:selected').attr("data-id");
                var reportList = JSON.parse(data);
				fillTable(reportList, status);
            },
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
        });
	});
	

	$('#yearList, #statusList').change(function() {
        $.ajax({
			url:'../apis/reportList.api.php',
			type:'POST',
			data: {	searchedVal: $('#searchedVal').val(),
					status: $('#statusList option:selected').attr("data-id"),
					year: $('#yearList option:selected').val()},
			
            success:function(data) {
				$('#tablebody').empty();
                var reportList = JSON.parse(data);
				var status = $('#statusList option:selected').attr("data-id");
				fillTable(reportList, status);
            },
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
        });
	});
	
	function fillTable(reportList, status) {
		for(i = 0; i < reportList.length; i++) {
			if (status == 1)
			{
				$('#tablebody').append('<tr><td>'+reportList[i]['id']+'</td><td>'+reportList[i]['username']+'</td><td>'+reportList[i]['year']+'</td><td><a href="editReport.php?id='+reportList[i]['id']+'&status='+status+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/declineReport.api.php?id='+reportList[i]['id']+'"><p class="fa fa-thumbs-down"></p>Ablehnen</a><a href="../apis/deactivateReport.api.php?id='+reportList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Deaktivieren</a></td>');
			}
			if (status == 2)
			{
				$('#tablebody').append('<tr><td>'+reportList[i]['id']+'</td><td>'+reportList[i]['username']+'</td><td>'+reportList[i]['year']+'</td><td><a href="editReport.php?id='+reportList[i]['id']+'&status='+status+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/acceptReport.api.php?id='+reportList[i]['id']+'"><p class="fa fa-thumbs-up"></p>Akzeptieren</a><a href="../apis/declineReport.api.php?id='+reportList[i]['id']+'"><p class="fa fa-thumbs-down"></p>Ablehnen</a><a href="../apis/deactivateReport.api.php?id='+reportList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Deaktivieren</a></td>');
			}
			if (status == 3)
			{
				$('#tablebody').append('<tr><td>'+reportList[i]['id']+'</td><td>'+reportList[i]['username']+'</td><td>'+reportList[i]['year']+'</td><td><a href="editReport.php?id='+reportList[i]['id']+'&status='+status+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/acceptReport.api.php?id='+reportList[i]['id']+'"><p class="fa fa-thumbs-up"></p>Akzeptieren</a><a href="../apis/deactivateReport.api.php?id='+reportList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Deaktivieren</a></td>');
			}
			if (status == 4)
			{
				$('#tablebody').append('<tr><td>'+reportList[i]['id']+'</td><td>'+reportList[i]['username']+'</td><td>'+reportList[i]['year']+'</td><td><a href="editReport.php?id='+reportList[i]['id']+'&status='+status+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/activateReport.api.php?id='+reportList[i]['id']+'"><p class="fa fa-thumbs-down"></p>Einreichen(Aktivieren)</a></td>');
			}
		}
	}
</script>
