﻿<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<div id="page-wrapper">
    <div class="row">
		<div class="col-lg-12">
            <h1 class="page-header">Templateliste</h1>
			<h3>Template erstellen</h3>
			
			<div class="row" style="margin-bottom:10px;">
				<label>Bilderanzahl</label>
				<div class="col-lg-2">
					<select class="form-control" id="imgcount">
						<option data-id="1">1</option>
						<option data-id="2">2</option>
						<option data-id="3">3</option>
						<option data-id="4">4</option>
					</select>
				</div>
			</div>
			<h4>HTML Code</h4>
        </div>
	</div>
	
    <div class="row">
		<div class="col-lg-6">
			<textarea class="form-control" rows="25" id="htmlcode"></textarea>
		</div>
		<div class="col-lg-6" style="border: 1px solid #66afe9; border-radius: 4px; height:515px;" id="showresult">
		</div>

    </div>
	
	<h4>LATEX Code</h4>
	<div class="row">
		<div class="col-lg-6">
			<textarea class="form-control" rows="25" id="latexcode"></textarea>
		</div>
    </div>
	<div class="col-lg-2" style="margin-top:5px; margin-bottom:20px;">
			<input type="submit" class="btn btn-primary" value="Template erstellen" id="saveButton">
	</div>
	<div class="col-lg-2" style="margin-top:5px; margin-bottom:20px;">
			<input type="submit" class="btn btn-primary" value="Verwerfen" id="discard">
	</div>
</div>
<script>
	$('#htmlcode').keyup(function(){
		$.ajax({
			success:function(data) {
				$('#showresult').empty();
				$value = $('#htmlcode').val();
				console.log($value);
				$('#showresult').append($value);
			},
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
		});
	});
	
	$('#saveButton').click(function(){
		$.ajax({
			url: "../apis/createTemplate.api.php",
			type: "POST",
			data: 	{
						imgcount: $('#imgcount option:selected').attr("data-id"),
						htmlcode: $('#htmlcode').val(),
						latexcode: $('#latexcode').val()
					},
			success:function(data) {
				//console.log(data);
				window.location.href = "../pages/template.php";
			},
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
		});
	});
	
	$('#discard').click(function(){
		$.ajax({
			success:function(data) {
				window.location.href = "../pages/template.php";
			},
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
		});
	});
</script>
<?php include("../content/head.inc.php"); ?>