﻿<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<div id="page-wrapper">
    <div class="row">
		<div class="col-lg-12">
            <h1 class="page-header">Templateliste</h1>
			<h3 id="id" data-id="<?php echo $_GET['id'] ?>">Template <?php echo $_GET['id'] ?> bearbeiten</h3>
			<h4>HTML Code</h4>
        </div>
	</div>
	
	<div class="row" style="margin-bottom:10px;">
		<label>Bilderanzahl</label>
		<div class="col-lg-2">
			<select class="form-control" id="imgcount">
				<option data-id="1">1</option>
				<option data-id="2">2</option>
				<option data-id="3">3</option>
				<option data-id="4">4</option>
			</select>
		</div>
	</div>
    <div class="row">

		<div class="col-lg-6">
			<textarea class="form-control" rows="25" id="htmlcode"></textarea>
		</div>
		<div class="col-lg-6">
			<label>HTML-Anzeige</label>
			<div style="border: 1px solid #66afe9; border-radius: 4px; height:29.7cm; width:21cm; transform: scale(0.7); transform-origin: left top;" id="showresult">
			</div>
		</div>
    </div>
	
	<h4>LATEX Code</h4>
	<div class="row">
		<div class="col-lg-6">
			<textarea class="form-control" rows="25" id="latexcode"></textarea>
		</div>
    </div>
	<div class="row">
		<div class="col-lg-2" style="margin-top:5px; margin-bottom:20px;">
			<input type="submit" class="btn btn-primary" value="Template bearbeiten" id="saveButton">
		</div>
		<div class="col-lg-2" style="margin-top:5px; margin-bottom:20px;">
			<input type="submit" class="btn btn-primary" value="Verwerfen" id="discard">
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$.ajax({
			url: "../apis/templateLoad.api.php",
			type: "POST",
			data: {id: $('#id').attr("data-id")},
			success:function(data) {
				var templateID = $('#id').attr("data-id");
				console.log(templateID);
                var templateList = JSON.parse(data);
				console.log(templateList);	
				$('#imgcount').val(templateList[0]['imgcount']);
				$('#htmlcode').val(templateList[0]['html_content']);
				$('#latexcode').val(templateList[0]['latex_content']);
				$value = $('#htmlcode').val();
				console.log($value);
				$('#showresult').append($value);
			},
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
		});
	});


	$('#htmlcode').keyup(function(){
		$.ajax({
			success:function(data) {
				$('#showresult').empty();
				$value = $('#htmlcode').val();
				console.log($value);
				$('#showresult').append($value);
			},
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
		});
	});
	
	$('#saveButton').click(function(){
		$.ajax({
			url: "../apis/saveTemplate.api.php",
			type: "POST",
			data: 	{
						id: $('#id').attr("data-id"),
						htmlcode: $('#htmlcode').val(),
						latexcode: $('#latexcode').val(),
						imgcount: $('#imgcount option:selected').attr("data-id"),
					},
			success:function(data) {
				//console.log(data);
				window.location.href = "../pages/template.php";
			},
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
		});
	});
	
	$('#discard').click(function(){
		$.ajax({
			success:function(data) {
				window.location.href = "../pages/template.php";
			},
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
		});
	});
</script>
<?php include("../content/head.inc.php"); ?>