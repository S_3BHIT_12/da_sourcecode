﻿<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<div id="page-wrapper">
    <div class="row">
		<div class="col-lg-12">
            <h1 class="page-header">Userliste</h1>
			<h3>User erstellen</h3>
        </div>
	</div>

    <div class="row">
		
        <div class="table-responsive col-lg-12"> <!-- -->
            <table class="table table-striped table-bordered table-hover">
                <thead> <!-- Head of Table-->
                    <tr>
                        <th>Benutzername</th>
                        <th>Passwort</th>
                        <th>E-Mail</th>
						<th>Telefonnummer</th>
						<th>Berechtigung</th>
						<th>Funktionen</th>
                    </tr>
                </thead>
                <tbody> <!-- Content of Table (document ready)-->
					<tr id="tablebody">					
						<td>
							<input id="username" placeholder="Max" class="form-control"/>
						</td>
						<td>
							<input id="password" placeholder="12345" class="form-control"/>
						</td>
						<td>
							<input id="email"  placeholder="max@gmail.com" class="form-control"/>
						</td>
						<td>
							<input id="phonenumber" placeholder="0123456789" class="form-control"/>
						</td>
						<td>
							<div class="form-group">
                                <select class="form-control" id="permissionList">

                                </select>
                            </div>
						</td>
						<td>
							<a id="saveButton" style="cursor:pointer">
							<p class="glyphicon glyphicon-ok"> </p>Speichern 
							</a>
							<a href="user.php">
							<p class="glyphicon glyphicon-remove"></p>Verwerfen
							</a>
						</td>
					</tr>	
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
	$(document).ready(function(){
		$.ajax({
			url: "../apis/permissiongroupList.api.php",
			success:function(data) {
				var permissiongroupList = JSON.parse(data);
				console.log(permissiongroupList);
				for(i=0; i < permissiongroupList.length; i++) {
					$('#permissionList').append('<option data-id="'+permissiongroupList[i]['id']+'">'+permissiongroupList[i]['name']+'</option>');
				}
			},
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
		});
	});
	
	$('#saveButton').click(function(){
		$.ajax({
			url: "../apis/createUser.api.php",
			type: "POST",
			data: 	{
						username: $('#username').val(),
						password: $('#password').val(),
						email: $('#email').val(),
						phonenumber: $('#phonenumber').val(),
						permission: $('#permissionList option:selected').attr("data-id")
					},
			success:function(data) {
				window.location.href = "../pages/user.php";
            },
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
		});
	});
</script>
<?php include("../content/head.inc.php"); ?>