﻿<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<div id="page-wrapper">
    <div class="row">
		<div class="col-lg-12">
            <h1 class="page-header">Userliste</h1>
			<h3>User bearbeiten</h3>
        </div>
	</div>

    <div class="row">
		
        <div class="table-responsive col-lg-12"> <!-- -->
            <table class="table table-striped table-bordered table-hover">
                <thead> <!-- Head of Table-->
                    <tr>
                        <th>ID</th>
                        <th>Benutzername</th>
                        <th>Passwort</th>
                        <th>E-Mail</th>
						<th>Telefonnummer</th>
						<th>Funktionen</th>
                    </tr>
                </thead>
                <tbody> <!-- Content of Table (document ready)-->
					<tr id="tablebody">
						<td id="id" data-id="<?php echo $_GET['id'] ?>">
							<?php echo $_GET['id'] ?>
						</td>
						
						<td>
							<input id="username" class="form-control"/>
						</td>
						<td>
							<input id="password" class="form-control"/>
						</td>
						<td>
							<input id="email"  class="form-control"/>
						</td>
						<td>
							<input id="phonenumber" class="form-control"/>
						</td>
						<td>
							<a id="saveButton" style="cursor:pointer">
							<p class="glyphicon glyphicon-ok"> </p>Speichern 
							</a>
							<a href="user.php">
							<p class="glyphicon glyphicon-remove"></p>Verwerfen
							</a>
						</td>
					</tr>	
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
	$(document).ready(function(){
		$.ajax({
			url: "../apis/userSelectedSearch.api.php",
			type: "POST",
			data: {id: $('#id').attr("data-id")},
			success:function(data) {
				var userID = $('#id').attr("data-id");
				console.log(userID);
                var userList = JSON.parse(data);
				console.log(userList);
				
				$('#username').val(userList[0]['username']);
				$('#password').val(userList[0]['password']);
				$('#email').val(userList[0]['email']);
				$('#phonenumber').val(userList[0]['phonenumber']);
            },
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
		});
	});
	
	$('#saveButton').click(function(){
		$.ajax({
			url: "../apis/saveUser.api.php",
			type: "POST",
			data: 	{	
						id: $('#id').text(),
						username: $('#username').val(),
						password: $('#password').val(),
						email: $('#email').val(),
						phonenumber: $('#phonenumber').val()
					},
			success:function(data) {
				console.log("successfully edited");
				//console.log(data);
				window.location.href = "../pages/user.php";
            },
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
		});
	});
</script>
<?php include("../content/head.inc.php"); ?>