<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<div id="page-wrapper">
    <div class="row">
		<div class="col-lg-12">
            <h1 id="head" class="page-header" data-id="<?php echo $_GET['year'] ?>">Jahresbericht erstellen</h1>
        </div>
	</div>
		
    <div class="row">
		<label>Berichtliste
		<select style="width:100px;" class="form-control" id="reportList">
		</select></label>
		<label>Jahrliste
        <select style="width:100px;" class="form-control" id="yearList">	
        </select></label><br>
		<input type="submit" style="width:200px; margin-top:10px;" class="btn btn-primary" value="Jahresbericht herunterladen" id="downloadYearlyReport">
		
		<div style="width:21cm; height:29.7cm; border: 1px solid black; margin:0 auto; margin-bottom: 20px;" id="showresult">
		</div>
    </div>
</div>
<?php include("../content/head.inc.php"); ?>
<script>
	var i = 0;
	var reportList;
    //document is loaded
    $(document).ready(function() {
		$.ajax({
            url:"../apis/yearList.api.php",
            success:function(data) {

                var yearList = JSON.parse(data);
				for(j = 0; j < yearList.length; j++) {
					$('#yearList').append('<option>'+yearList[j]['year']+'</option>');
				}
				$('#yearList').val((new Date).getFullYear());
				getReportList($('#yearList option:selected').val());
				
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
	});
	
	$('#downloadYearlyReport').click(function() {
		console.log(reportList);
		window.location.href = "../apis/fillYearlyReport.api.php?reportList="+JSON.stringify(reportList)+"&year="+$('#yearList option:selected').val()+"";
		console.log(reportList);
	});
	
	$('#reportList').change(function(){
		i = $('#reportList option:selected').val();
		fillTemplate(reportList, i);
	});
	
	$('#yearList').change(function(){
		getReportList($('#yearList option:selected').val());
		i = 0;
	});
	
	function getReportList(year) {
		$.ajax({
            url:"../apis/yearlyReportList.api.php",
			type: "POST",
			data: { year: year},
            success:function(data) {
                reportList = JSON.parse(data);
				fillTemplate(reportList, i); //wegen Objekt 0
				fillreportList(reportList, i); //wegen Objekt 0
				console.log(data);
            },
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
        });
	}
	
	//FILL THAT TEMPLATE ON KEY UP - DOCUMENT READY
	function fillTemplate(reportList, i) {
		console.log(i);
		console.log(reportList);
		$.ajax({
			url: "../apis/fillTemplate.api.php",
			type: "POST",
			data: {	heading: reportList[i]['heading'],
					content: reportList[i]['content'],
					template: reportList[i]['template_id'],
					report: reportList[i]['id'] },
			success:function(data) {
				$('#showresult').empty();
				$('#showresult').append(data);
			},
			error:function(data,textStatus,errorThrown) {
				alert(data.responseText);
			}
		});
	}
	
	function fillreportList(reportList, i) {
	//console.log(reportList);
		$('#reportList').empty();
		for(j = 0; j < reportList.length; j++) {
			$('#reportList').append('<option>'+j+'</option>');
			$('#reportList option:selected').val(i);
		}
	}
</script>