<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<div id="page-wrapper">
    <div class="row"><div class="col-lg-12">
            <h1 class="page-header">Templateliste</h1>
        </div></div>

    <div class="row">
		
		<div class="col-lg-6" style="margin-bottom: 10px;"> 
			<div class="checkbox">
                <label>
					<input id="showdeleted" type="checkbox" value="">Archivierte anzeigen
                </label>
            </div>
		</div>
		
        <div class="table-responsive col-lg-12"> <!-- -->
            <table class="table table-striped table-bordered table-hover">
                <thead> <!-- Head of Table-->
                    <tr>
                        <th>ID</th>
						<th>Funktionen</th>
                    </tr>
                </thead>
                <tbody id="tablebody"> <!-- Content of Table (document ready)-->
					<tr>
					</tr>
                </tbody>
            </table>
			<input type="submit" class="btn btn-primary" value="Template erstellen" id="createTemplate" onclick="window.location.href='../pages/createTemplate.php'">
        </div>

    </div>
</div>
<?php include("../content/head.inc.php"); ?>

<script>

    //document is loaded
    $(document).ready(function() {
        $.ajax({
            url:"../apis/templateList.api.php",
            success:function(data) {

                var templateList = JSON.parse(data);
				console.log(templateList);
				for(i = 0; i < templateList.length; i++) {
					if(templateList[i]['deleted'] == 0)
					{
						$('#tablebody').append('<tr><td>'+templateList[i]['id']+'</td><td><a href="editTemplate.php?id='+templateList[i]['id']+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/deactivateTemplate.api.php?id='+templateList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Deaktivieren</a></td></tr>');
					}
				}
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
	});
	
	$('#showdeleted').change(function() {
        $.ajax({
            url:"../apis/templateList.api.php",
            success:function(data) {
			
				$('#tablebody').empty();
				
                var templateList = JSON.parse(data);
				var checkbox = $('#showdeleted').is(":checked");
				for(i = 0; i < templateList.length; i++) {
					console.log(templateList[i]);
					if(checkbox == true && templateList[i]['deleted'] == 1)
					{
						console.log("in deleted");
						$('#tablebody').append('<tr><td>'+templateList[i]['id']+'</td><td><a href="editTemplate.php?id='+templateList[i]['id']+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/activateTemplate.api.php?id='+templateList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Aktivieren</a></td></tr>');
					}
					
					if(checkbox == false && templateList[i]['deleted'] == 0)
					{
						$('#tablebody').append('<tr><td>'+templateList[i]['id']+'</td><td><a href="editTemplate.php?id='+templateList[i]['id']+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/deactivateTemplate.api.php?id='+templateList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Deaktivieren</a></td></tr>');
					}
				}
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
	});
</script>
