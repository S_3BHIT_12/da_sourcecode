<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<div id="page-wrapper">
    <div class="row"><div class="col-lg-12">
            <h1 class="page-header">Userliste</h1>
        </div></div>

    <div class="row">
	
        <div class="col-lg-12" style="margin-bottom: 10px;"> <!--Search input -->
			<div class="input-group custom-search-form col-lg-12">
                <input id="searchedVal" type="text" class="form-control">
            </div>
		</div> <!-- Search input-->
		
		<div class="col-lg-6" style="margin-bottom: 10px;"> 
			<div class="checkbox">
                <label>
					<input id="showdeleted" type="checkbox" value="">Archivierte anzeigen
                </label>
            </div>
		</div>
		
        <div class="table-responsive col-lg-12"> <!-- -->
            <table class="table table-striped table-bordered table-hover">
                <thead> <!-- Head of Table-->
                    <tr>
                        <th>ID</th>
                        <th>Benutzername</th>
                        <th>Passwort</th>
                        <th>E-Mail</th>
						<th>Telefonnummer</th>
						<th>Funktionen</th>
                    </tr>
                </thead>
                <tbody id="tablebody"> <!-- Content of Table (document ready)-->
					<tr>
					</tr>
                </tbody>
            </table>
			<input type="submit" class="btn btn-primary" value="User erstellen" id="createUser" onclick="window.location.href='../pages/createUser.php'">
        </div>

    </div>
</div>
<?php include("../content/head.inc.php"); ?>

<script>

    //document is loaded
    $(document).ready(function() {
        $.ajax({
            url:"../apis/userList.api.php",
            success:function(data) {

                var userList = JSON.parse(data);
				
				for(i = 0; i < userList.length; i++) {
					if(userList[i]['deleted'] == 0)
					{
						$('#tablebody').append('<tr><td>'+userList[i]['id']+'</td><td>'+userList[i]['username']+'</td><td>'+userList[i]['password']+'</td><td>'+userList[i]['email']+'</td><td>'+userList[i]['phonenumber']+'</td><td><a href="editUser.php?id='+userList[i]['id']+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/deactivateUser.api.php?id='+userList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Deaktivieren</a></td>');
					}
				}
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
	});

	$('#searchedVal').keyup(function() {
        $.ajax({
			url:'../apis/userSearch.api.php',
			type:'POST',
			data: {searchedVal: $('#searchedVal').val()},
			
            success:function(data) {
			$('#tablebody').empty();
				console.log(data);
				var userList = JSON.parse(data);
				var checkbox = $('#showdeleted').is(":checked");

				for(i=0; i < userList.length; i++) {
					if(checkbox == true && userList[i]['deleted'] == 1)
					{
						$('#tablebody').append('<tr><td>'+userList[i]['id']+'</td><td>'+userList[i]['username']+'</td><td>'+userList[i]['password']+'</td><td>'+userList[i]['email']+'</td><td>'+userList[i]['phonenumber']+'</td><td><a href="editUser.php?id='+userList[i]['id']+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/activateUser.api.php?id='+userList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Aktivieren</a></td></tr>');
					}
					
					if(checkbox == false && userList[i]['deleted'] == 0)
					{
						$('#tablebody').append('<tr><td>'+userList[i]['id']+'</td><td>'+userList[i]['username']+'</td><td>'+userList[i]['password']+'</td><td>'+userList[i]['email']+'</td><td>'+userList[i]['phonenumber']+'</td><td><a href="editUser.php?id='+userList[i]['id']+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/deactivateUser.api.php?id='+userList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Deaktivieren</a></td></tr>');
					}
				}
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
	});
	
	$('#showdeleted').change(function() {
        $.ajax({
            url:"../apis/userList.api.php",
            success:function(data) {
			
				$('#tablebody').empty();
				
                var userList = JSON.parse(data);
				var checkbox = $('#showdeleted').is(":checked");
				
				for(i = 0; i < userList.length; i++) {
					if(checkbox == true && userList[i]['deleted'] == 1)
					{
						$('#tablebody').append('<tr><td>'+userList[i]['id']+'</td><td>'+userList[i]['username']+'</td><td>'+userList[i]['password']+'</td><td>'+userList[i]['email']+'</td><td>'+userList[i]['phonenumber']+'</td><td><a href="editUser.php?id='+userList[i]['id']+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/activateUser.api.php?id='+userList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Aktivieren</a></td>');
					}
					if(checkbox == false && userList[i]['deleted'] == 0)
					{
						$('#tablebody').append('<tr><td>'+userList[i]['id']+'</td><td>'+userList[i]['username']+'</td><td>'+userList[i]['password']+'</td><td>'+userList[i]['email']+'</td><td>'+userList[i]['phonenumber']+'</td><td><a href="editUser.php?id='+userList[i]['id']+'"><p class="fa fa-edit"> </p>Bearbeiten </a><a href="../apis/deactivateUser.api.php?id='+userList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Deaktivieren</a></td>');
					}
				}
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
	});
</script>
