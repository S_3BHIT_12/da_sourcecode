<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<div id="page-wrapper">
    <div class="row">
		<div class="col-lg-12">
            <h1 class="page-header">Berichtliste</h1>
			<h3 id="reportid" data-id="<?php echo $_GET['id'] ?>">Bericht <?php echo $_GET['id'] ?> bearbeiten</h3>
        </div>
	</div>
	
    <div class="row">
		<div class="col-lg-6">		
			<button class="btn btn-primary btn-default dropdown-toggle" type="button" id="dropdownMenuButton_edit_Attributes" data-toggle="modal" data-target="#edit_Attributes_popup">Tabellenattribute editieren</button>
 
			<div class="modal fade" id="edit_Attributes_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
							<label>Tabelle</label>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Zeilenname</th>
										<th>Zeilenwert</th>
										<th>Funktionen</th>
									</tr>
								</thead>
								<tbody id="tablebody">
									<tr>					

									</tr>
								</tbody>
							</table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
			
			<br>
			<label>Überschrift</label>
			<input id="heading" placeholder="Überschrift" class="form-control">
			<label>Inhalt</label>
			<textarea class="form-control" rows="10" id="content"></textarea>
			<label>Template</label>
			<select class="form-control" id="templateList">
			</select>
			
			<div style="margin-top:10px">
				<input type="submit" class="btn btn-primary" value="Bericht bearbeiten" id="saveButton">

				<input type="submit" class="btn btn-primary" value="Verwerfen" id="discard">
			</div>
		</div>
		
		<div class="col-lg-6">
			<label>HTML</label>
			<div style="border: 1px solid #66afe9; border-radius: 4px; height:29.7cm; width:21cm; transform: scale(0.7); transform-origin: left top;" id="showresult">
			</div>
		</div>
    </div>

</div>
<script>
	$(document).ready(function(){
	//TEMPLATELISTE BEFÜLLEN
		$.ajax({
			url: "../apis/templateList.api.php",
			success:function(data) {
                var templateList = JSON.parse(data);
				//console.log(templateList);
			
				for(i = 0; i < templateList.length; i++) {
					if(templateList[i]['deleted'] == false)
					{
						$('#templateList').append('<option>'+templateList[i]['id']+'</option>');
					}
				}
				loadAttributes();
			},
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
		});
	});
	
	function loadAttributes() {
		$.ajax({
			url: "../apis/attributeList.api.php",
			type: "POST",
			data: {id: $('#reportid').attr("data-id")},
			success:function(data) {
				//console.log($('#reportid').attr("data-id"));
				var attributeList = JSON.parse(data);
				//console.log(attributeList);
				fillAttribute(attributeList);
				loadReport();
			},
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
		});
	}
	
	function loadReport() {
		$.ajax({
			url: "../apis/reportLoad.api.php",
			type: "POST",
			data: {id: $('#reportid').attr("data-id")},
			success:function(data) {
                var reportList = JSON.parse(data);
				console.log(reportList);
			
				$('#heading').val(reportList[0]['heading']);
				$('#content').val(reportList[0]['content']);
				$('#templateList').val(reportList[0]['id']);
				fillTemplate();
			},
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
		});
	}
	
	
	function deleteAttribute(id) {
		$.ajax({
			url: "../apis/deleteAttribute.api.php",
			type: "POST",
			data: 	{
						id: id,
						reportid: $('#reportid').attr("data-id")
					},
			success:function(data) {
				var attributeList = JSON.parse(data);
				fillAttribute(attributeList);
			},
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
		});
	}
	
	function createAttribut() {
		$.ajax({
			url: "../apis/createAttribute.api.php",
			type: "POST",
			data: 	{
						reportid: $('#reportid').attr("data-id"),
						name: $('#attrname').val(),
						value: $('#attrvalue').val()
					},
			success:function(data) {
				var attributeList = JSON.parse(data);
				console.log(attributeList);
				fillAttribute(attributeList);
			},
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
		});
	}
		
//KEY UP ON THAT TWO DIVS
	$('#heading, #content').keyup(function(){
		fillTemplate();
	});
	
	$('#templateList').change(function(){
		fillTemplate();
	});
	
//FILL THAT TEMPLATE ON KEY UP - DOCUMENT READY
	function fillTemplate() {
		$.ajax({
			url: "../apis/fillTemplate.api.php",
			type: "POST",
			data: {	heading: $('#heading').val(),
					content: $('#content').val(),
					template: $('#templateList').val(),
					report: $('#reportid').attr("data-id") },
			success:function(data) {
				console.log(data);
				$('#showresult').empty();
				$('#showresult').append(data);
			},
			error:function(data,textStatus,errorThrown) {
				alert(data.responseText);
			}
		});
	}
	
	function fillAttribute(attributeList) {
		$('#tablebody').empty();
		
		for(i = 0; i < attributeList.length; i++) {
			$('#tablebody').append('<tr><td><input id="name" class="form-control" value="'+attributeList[i]['name']+'"></td><td><input id="value" class="form-control" value="'+attributeList[i]['value']+'"></td><td><a onclick="deleteAttribute('+attributeList[i]['id']+')" style="cursor:pointer;"><p class="glyphicon glyphicon-remove"></p>Löschen</a></td></tr>');
		}
		$('#tablebody').append('<tr><td><input id="attrname" class="form-control"></td><td><input id="attrvalue" class="form-control"></td><td><a onclick="createAttribut()" style="cursor:pointer;"><p class="glyphicon glyphicon-ok"></p>Erstellen</a></td></tr>');
		fillTemplate();
	}
	
	$('#saveButton').click(function(){
		$.ajax({
			url: "../apis/saveReport.api.php",
			type: "POST",
			data: 	{
						heading: $('#heading').val(),
						content: $('#content').val(),
						template: $('#templateList').val(),
						reportid: $('#reportid').attr("data-id")
					},
			success:function(data) {
				window.location.href = "../pages/report.php";
			},
            error:function(data,textStatus,errorThrown) {
                alert(data.responseText);
            }
		});
	});
	
	$('#discard').click(function(){
		$.ajax({
			success:function(data) {
				window.location.href = "../pages/report.php";
			},
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
		});
	});
</script>
<?php include("../content/head.inc.php"); ?>