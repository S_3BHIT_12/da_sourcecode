<?php
session_start();
require('/Backend/content/db.inc.php');
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	
	$res = $db->prepare("Select id from user where username = :uusername and password = :upassword;");
	$username = $_GET['username'];
	$password = $_GET['password'];
	$res->bindParam(':uusername', $username, PDO::PARAM_STR);
	$res->bindParam(':upassword', $password, PDO::PARAM_STR);
	$res->execute();
	//id of selected user
	$id = $res->fetch()[0];
	
	$del = $db->prepare("Select deleted from user where username = :uusername and password = :upassword;");
	$del->bindParam(':uusername', $username, PDO::PARAM_STR);
	$del->bindParam(':upassword', $password, PDO::PARAM_STR);
	$del->execute();

	$deleted = $del->fetch()[0];
	
	
	//if user doesn't exist
	if ($id == 0)
	{
		//if doesn't exist go back to login and throw Error-Message
		header ("Location: login.php?error=1");
	}
	
	//if user is inavtivated
	if ($deleted == 1)
	{
		//if inavtivated go back to login and throw Error-Message
		header ("Location: login.php?error=2");
	}
	
	//if user exists
	if ($id != 0 && $deleted != 1)
	{
		$res1 = $db->prepare("Select permissiongroup_id from user_has_permissiongroup where user_id = :uid;");
		$res1->bindParam(':uid', $id, PDO::PARAM_STR);
		$res1->execute();
		//group id -- 1 = USER -- 2 = ADMIN
		$group = $res1->fetch()[0];
		//USER
		if($group == 1)
		{
			$_SESSION['user_id'] = $id; //user session
			header("Location:index.php");
		}
		
		//ADMIN
		if($group == 2)
		{
			$_SESSION['admin_id'] = $id; //admin session
			header("Location:index.php");
		}
	}
?>