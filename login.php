<?php include("/Backend/content/head.inc.php"); ?>
<?php
	if (isset($_GET['error']))
		{
		$err = $_GET['error']; 
		if ($err == 1)
			{?>
				<div class="alert alert-danger">
					<strong>Achtung!</strong> Benutzername und/oder Passwort stimmen nicht miteinander überein!
					<a href="#"> Passwort vergessen?</a>
				</div>
<?php 		}
		if($err == 2)
		{?>	
			<div class="alert alert-danger">
					<strong>Achtung!</strong> Benutzer ist nicht aktiviert!
			</div>
<?php	}
	}?>
<div class="container">
        <div class="col-md-4 col-md-offset-4" style="margin-top: 200px">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Bitte Einloggen</h3>
                </div>
                <div class="panel-body">
					 <fieldset>
						<form class="form-signin" method="GET" action="logincheck.php">
                       
                             <div class="form-group">
                                <input class="form-control" placeholder="Benutzername" name="username" type="text" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                                <!-- Change this to a button or input when using this as a form -->
                            <button class="btn btn-lg btn-success ">Login</button>
						</form>
					</fieldset>
                </div>
            </div>
        </div>
    </div>
<?php
	include "/Backend/content/footer.inc.php";
?>