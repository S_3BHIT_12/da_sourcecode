<?php
/* Smarty version 3.1.30, created on 2017-03-22 13:52:50
  from "4622457b8bbd3f332ba20952d1fc69cbaceed767" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d273a202b8a5_61136860',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58d273a202b8a5_61136860 (Smarty_Internal_Template $_smarty_tpl) {
?>
<html>
<style>
table, td, th {
border: 1px solid #ddd;
text-align: left;
}
table {
border-collapse: collapse;
width: 100%;
}
th, td {
padding: 15px;
}
.heading{
font-family: verdana;
font-size: 300%;
margin: 2%;
}
.content{
margin:2.5%;
font-family: arial;
font-size: 120%;
}
.img{
background-size: 100% auto;
position: relative;
width: 5cm;
height: 5cm;
padding:2%;
}
</style>

<body>
<table><?php echo $_smarty_tpl->tpl_vars['table']->value;?>
</table>
<h1 class="heading"><?php echo $_smarty_tpl->tpl_vars['u1']->value;?>
</h1>
<p class="content"><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</p>
<img class="img" <?php echo $_smarty_tpl->tpl_vars['image0']->value;?>
>
<img class="img" <?php echo $_smarty_tpl->tpl_vars['image1']->value;?>
>
<img class="img" <?php echo $_smarty_tpl->tpl_vars['image2']->value;?>
>
</body>
</html><?php }
}
