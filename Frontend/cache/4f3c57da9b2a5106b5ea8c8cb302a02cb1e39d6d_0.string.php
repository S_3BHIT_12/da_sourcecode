<?php
/* Smarty version 3.1.30, created on 2017-03-22 20:43:33
  from "4f3c57da9b2a5106b5ea8c8cb302a02cb1e39d6d" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d2d3e588ab67_71291294',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58d2d3e588ab67_71291294 (Smarty_Internal_Template $_smarty_tpl) {
?>
<html>
<style>
table, td, th {
border: 1px solid #ddd;
text-align: left;
}
table {
border-collapse: collapse;
width: 100%;
}
th, td {
padding: 15px;
}
.heading{
font-family: verdana;
font-size: 300%;
margin: 2%;
}
.content{
margin:2.5%;
font-family: arial;
font-size: 120%;
}
.img{
background-size: 100% auto;
position: relative;
width: 4cm;
height: 4cm;
padding:2%;
}
</style>

<body>
<table><?php echo $_smarty_tpl->tpl_vars['table']->value;?>
</table>
<h1 class="heading"><?php echo $_smarty_tpl->tpl_vars['u1']->value;?>
</h1>
<p class="content"><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</p>
<img class="img" <?php echo $_smarty_tpl->tpl_vars['image0']->value;?>
>
<img class="img" <?php echo $_smarty_tpl->tpl_vars['image1']->value;?>
>
<img class="img" <?php echo $_smarty_tpl->tpl_vars['image2']->value;?>
>
</body>
</html><?php }
}
