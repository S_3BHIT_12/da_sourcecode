<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Diplomarbeit >>Tut Gut!<<">
    <meta name="author" content="Pfeffer Daniel und Moser Tobias">

    <!-- Bootstrap Core CSS -->
    <link href="/da_sourcecode/Frontend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	

    <!-- Custom Fonts -->
    <link href="/da_sourcecode/Frontend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Layout CSS -->
    
	
    <!-- jQuery -->
    <script src="/da_sourcecode/Frontend/vendor/jquery/jquery.min.js"></script>
	<script src="/da_sourcecode/Frontend/vendor/bootstrap/js/bootstrap.min.js"></script>
	
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
	
	<link href="/da_sourcecode/Frontend/layout.css" rel="stylesheet">