<?php
try {
    $url = "localhost";
    $dbName = "jahresbericht";
    $username = "root";
    $passwd = "";

	//--SMARTY--
	require_once "/../vendor/smarty-3.1.30/libs/Smarty.class.php";
	$smarty = new Smarty();
	$smarty->template_dir = '../templates';
	$smarty->compile_dir = '../cache';
	
    $db = new PDO ("mysql:host=".$url.";dbname=".$dbName,$username,$passwd);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}
catch(PDOException $e) {
    header("HTTP/1.1 503 Service Unavailable");
    die();
}