<?php
class user
{
	private $db;
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	
	//getCurrent User
	public function getCurrentUser()
	{
		$currentUser = $this->db->prepare('select username from user where id=:currentUser');
		$currentUser->bindParam(':currentUser', $_SESSION['user_id'], PDO::PARAM_STR);
		$currentUser->execute();
		return $currentUser->fetch(PDO::FETCH_ASSOC);
	}
}