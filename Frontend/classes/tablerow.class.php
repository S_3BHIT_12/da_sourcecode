<?php
class tablerow
{
	private $db;
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function getTableRows($id)
	{
		$stmt = $this->db->query('SELECT * from table_row WHERE report_id = '.$id.'');
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}
	
	public function deletetablerow($id)
	{
		$stmt = $this->db->query('DELETE FROM table_row WHERE id = '.$id.'');
		return $stmt;
	}
	
	public function deleterows($reportid)
	{
		$stmt = $this->db->prepare('DELETE FROM table_row WHERE report_id = :reportid');
		$stmt->bindParam(':reportid', $reportid, PDO::PARAM_STR);
		$stmt->execute();
		return $stmt;
	}
	
	public function inserttablerow($rowname, $rowval, $reportid)
	{
		$insrow = $this->db->prepare('INSERT INTO table_row(name, value, report_id) VALUES(:name, :val, :reportid)');
		$insrow->bindParam(':name', $rowname, PDO::PARAM_STR);
		$insrow->bindParam(':val', $rowval, PDO::PARAM_STR);
		$insrow->bindParam(':reportid', $reportid, PDO::PARAM_STR);
		$insrow->execute();
		return $rowname;
	}
	
	public function newtablerow($newtablerowname, $newtablerowval, $reportid)
	{
		$newtableentry = $this->db->prepare("INSERT INTO table_row(name, value, report_id) VALUES(:name, :val, :reportid)");
		$newtableentry->bindParam(':name', $newtablerowname, PDO::PARAM_STR);
		$newtableentry->bindParam(':val', $newtablerowval, PDO::PARAM_STR);
		$newtableentry->bindParam(':reportid', $reportid, PDO::PARAM_STR);
	
		$newtableentry->execute();
		return $newtableentry;
	}
}