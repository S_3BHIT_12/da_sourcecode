<?php
class status
{
	private $db;
	
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	public function getStatus()
	{
		session_start();

		date_default_timezone_set("Europe/Berlin");
		$timestamp = time();
	
		$year = date("Y",$timestamp);
		
		$curruser = $_SESSION['user_id'];
		
		$getreportid = $this->db->prepare("SELECT id from report where user_id=:curruser and year=:curryear");
		$getreportid->bindParam(':curruser', $curruser);
		$getreportid->bindParam(':curryear', $year);
		$getreportid->execute();
		while($row = $getreportid->fetch(PDO::FETCH_ASSOC)){
			$reportid = $row['id'];
		}
		
		$stmt = $this->db->query('SELECT accepted, submitted, deleted from status where report_id='.$reportid.'');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	public function getReportStatus()
	{
		session_start();

		date_default_timezone_set("Europe/Berlin");
		$timestamp = time();
	
		$year = date("Y",$timestamp);
		
		$curruser = $_SESSION['user_id'];
		
		$getreportid = $this->db->prepare("SELECT id from report where user_id=:curruser and year=:curryear");
		$getreportid->bindParam(':curruser', $curruser);
		$getreportid->bindParam(':curryear', $year);
		$getreportid->execute();
		while($row = $getreportid->fetch(PDO::FETCH_ASSOC)){
			$reportid = $row['id'];
		}
		
		$stmt = $this->db->query('SELECT * from status s, report r where s.report_id='.$reportid.' AND r.id='.$reportid.'');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
		
	}
}