<?php
class image
{
	private $db;
	
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	public function getimgage()
	{		
	
		//session_start();
		date_default_timezone_set("Europe/Berlin");
		$timestamp = time();
	
		$year = date("Y",$timestamp);
		
		$curruser = $_SESSION['user_id'];
		
		$getreportid = $this->db->prepare("SELECT id from report where user_id=:curruser and year=:curryear");
		$getreportid->bindParam(':curruser', $curruser);
		$getreportid->bindParam(':curryear', $year);
		$getreportid->execute();
		while($row = $getreportid->fetch(PDO::FETCH_ASSOC)){
			$reportid = $row['id'];
		}
		$getimg = $this->db->query('SELECT * FROM image WHERE report_id='.$reportid.'');
		return $getimg->fetchAll(PDO::FETCH_ASSOC);	
	}
	public function getimage()
	{		
	
		session_start();
		date_default_timezone_set("Europe/Berlin");
		$timestamp = time();
	
		$year = date("Y",$timestamp);
		
		$curruser = $_SESSION['user_id'];
		
		$getreportid = $this->db->prepare("SELECT id from report where user_id=:curruser and year=:curryear");
		$getreportid->bindParam(':curruser', $curruser);
		$getreportid->bindParam(':curryear', $year);
		$getreportid->execute();
		while($row = $getreportid->fetch(PDO::FETCH_ASSOC)){
			$reportid = $row['id'];
		}
		$getimg = $this->db->query('SELECT * FROM image WHERE report_id='.$reportid.'');
		return $getimg->fetchAll(PDO::FETCH_ASSOC);	
	}
	public function deleteimg($id)
	{
		$stmt = $this->db->query('DELETE FROM image WHERE id = '.$id.'');
		return $stmt;
	}
}