<?php
class template
{
	private $db;
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function getTemplates()
	{
		$stmt = $this->db->query('SELECT id FROM template where deleted=FALSE');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function getTempContent($id)
	{
		$stmt = $this->db->query("select html_content from template where id=".$id." and deleted=FALSE");
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function templateLoad($id)
	{
		$stmt = $this->db->prepare('SELECT * from template where id=:id');
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
}