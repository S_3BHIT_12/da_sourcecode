<?php
class report
{
	private $db;
	
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	public function createReport()
	{
		session_start();

		date_default_timezone_set("Europe/Berlin");
		$timestamp = time();
	
		$year = date("Y",$timestamp);
		
		$curruser = $_SESSION['user_id'];
		
		$statusbool = 0;
		
		$stmt = $this->db->prepare("INSERT INTO report(user_id, year, template_id) VALUES(:user_id, :year, 1)");
		$stmt->bindParam(':user_id', $curruser);
		$stmt->bindParam(':year', $year, PDO::PARAM_STR);
		$stmt->execute();
		
		$getreport = $this->db->prepare("SELECT id from report where user_id=:curruser and year=:curryear");
		$getreport->bindParam(':curruser', $curruser);
		$getreport->bindParam(':curryear', $year);
		$getreport->execute();
		while($row = $getreport->fetch(PDO::FETCH_ASSOC)){
			$reportid = $row['id'];
		}
		
		//Status erstellen
		$savestatus = $this->db->prepare("INSERT INTO status(accepted, submitted, declined, deleted, report_id) VALUES(:acc, :sub, :dec, :del, :reportid)");
		$savestatus->bindParam(':acc', $statusbool, PDO::PARAM_STR);
		$savestatus->bindParam(':sub', $statusbool, PDO::PARAM_STR);
		$savestatus->bindParam(':dec', $statusbool, PDO::PARAM_STR);
		$savestatus->bindParam(':del', $statusbool, PDO::PARAM_STR);
		$savestatus->bindParam(':reportid', $reportid, PDO::PARAM_STR);
		$savestatus->execute();
		
		$getusername = $this->db->prepare("SELECT username FROM user WHERE id=:userid");
		$getusername->bindParam(':userid', $curruser);
		$getusername->execute();
		while($row = $getusername->fetch(PDO::FETCH_ASSOC)){
			$username = $row['username'];
		}
		
		$target_dir = "../uploads/".$username."/";

		if(!file_exists($target_dir) && !is_dir($target_dir)){
			mkdir($target_dir,0777);
		}
		return $savestatus;
	}
	public function getReport()
	{
		session_start();

		date_default_timezone_set("Europe/Berlin");
		$timestamp = time();
	
		$year = date("Y",$timestamp);
		
		$curruser = $_SESSION['user_id'];
		
		$stmt = $this->db->query('SELECT r.id, r.heading, r.content, r.template_id FROM report r, status s WHERE r.year='.$year.' AND r.user_id='.$curruser.' AND r.id = s.report_id AND s.deleted = FALSE AND s.accepted = FALSE AND s.submitted = FALSE');
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}
	
	public function attributeList()
	{
		session_start();
		
		date_default_timezone_set("Europe/Berlin");
		$timestamp = time();
	
		$year = date("Y",$timestamp);
		
		$curruser = $_SESSION['user_id'];
		
		$getreportid = $this->db->prepare("SELECT id from report where user_id=:curruser and year=:curryear");
		$getreportid->bindParam(':curruser', $curruser);
		$getreportid->bindParam(':curryear', $year);
		$getreportid->execute();
		while($row = $getreportid->fetch(PDO::FETCH_ASSOC)){
			$reportid = $row['id'];
		}
		
		$stmt = $this->db->query('SELECT * from table_row where report_id = '.$reportid.'');
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function checkifreportalreadyexits()
	{
		session_start();
		
		date_default_timezone_set("Europe/Berlin");
		$timestamp = time();
	
		$year = date("Y",$timestamp);
		
		$curruser = $_SESSION['user_id'];
		
		$getreportid = $this->db->prepare("SELECT COUNT(*) from report r, status s where r.user_id=:curruser and r.year=:curryear and s.deleted=false");
		$getreportid->bindParam(':curruser', $curruser);
		$getreportid->bindParam(':curryear', $year);
		$getreportid->execute();
		return $getreportid->fetchColumn();
		//while($row = $getreportid->fetch(PDO::FETCH_ASSOC)){
			//$reportid = $row['id'];
		//}
		//return $reportid;
	}
	public function checkifreportexits()
	{
		session_start();
		
		date_default_timezone_set("Europe/Berlin");
		$timestamp = time();
	
		$year = date("Y",$timestamp);
		
		$curruser = $_SESSION['user_id'];
		
		$getreportid = $this->db->prepare("SELECT COUNT(*) from report where user_id=:curruser and year=:curryear");
		$getreportid->bindParam(':curruser', $curruser);
		$getreportid->bindParam(':curryear', $year);
		$getreportid->execute();
		return $getreportid->fetchColumn();
	}
}