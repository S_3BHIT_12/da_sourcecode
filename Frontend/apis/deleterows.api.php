<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/tablerow.class.php');

// Klassen instanziieren
$tablerow = new tablerow($db);

if(!empty($_POST['reportid']))
{
	$tablerow->deleterows($_POST['reportid']);
}
else{
	http_response_code(422);
    echo "Invalid parameters given!";
}