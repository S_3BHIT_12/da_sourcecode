<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/tablerow.class.php');

// Klassen instanziieren
$tablerow = new tablerow($db);

//if empty
if(!empty($_GET['id']))
{
	$tablerow->deletetablerow($_GET['id']);
	header("location: ../pages/existingreport.php");
}
else{
	http_response_code(422);
    echo "Invalid parameters given!";
}