<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/user.class.php');

// Klassen instanziieren
session_start();
$user = new user($db);

if(isset($_SESSION['user_id'])){
	echo json_encode($user->getCurrentUser());
}
else{
	echo "Keine ID gesetzt";
}