<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/report.class.php');

// Klassen instanziieren
session_start();
$report = new report($db);

echo json_encode($report->getYear());