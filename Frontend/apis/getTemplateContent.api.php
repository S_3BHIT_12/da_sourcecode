<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/template.class.php');

// Klassen instanziieren
$template = new template($db);

if(!empty($_POST['id']))
{
	echo json_encode($template->getTempContent($_POST['id']));
}

else
{
	http_response_code(422);
    echo "Invalid parameters given!";
}