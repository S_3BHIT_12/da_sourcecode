<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/report.class.php');

// Klassen instanziieren
session_start();
$report = new report($db);

$report->saveData( $_POST['template_id'], $_POST['titel'], $_POST['year'], $_POST['description']);