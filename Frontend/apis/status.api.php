<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/status.class.php');

// Klassen instanziieren
$status = new status($db);

if(!empty($_POST['id']))
{
	$status->createStatus($_POST['id']);
}
else
{
	http_response_code(422);
    echo "Invalid parameters given!";
}