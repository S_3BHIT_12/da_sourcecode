<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/template.class.php');
include('../classes/report.class.php');
include('../classes/image.class.php');

// Klassen instanziieren
$template = new template($db);
$report = new report($db);
$image = new image($db);

$images = $image->getimage();

$selectedtemplate = $template->templateLoad($_POST['templateid']);
if($selectedtemplate[0]['imgcount'] <= count($images)){
	$buildtable = '';
	$buildtable = $buildtable . '<tr><td>'.$_POST['gname'].'</td><td>'.$_POST['gval'].'</td></tr><tr><td>'.$_POST['plz'].'</td><td>'.$_POST['plzval'].'</td></tr><tr><td>'.$_POST['bev'].'</td><td>'.$_POST['bevval'].'</td></tr>';

	for($i = 0; $i < count($images); $i++){
		$img = 'src="data:image/gif;base64,'.$images[$i]['code'].'"';

		$image = 'image'.$i;
		$smarty->assign($image, $img);
	}

	$smarty->assign('table', $buildtable);
	$smarty->assign('u1', $_POST['heading']);
	$smarty->assign('content', $_POST['content']);
	$html_content = $selectedtemplate[0]['html_content'];
	$smarty->display('string:'.$html_content);
}
else
	echo "<strong>Achtung!</strong>Es befinden sich zu wenig Fotos im Bericht, um dieses Template auszuwählen!!";