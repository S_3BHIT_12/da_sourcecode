<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/template.class.php');

// Klassen instanziieren
session_start();
$template = new template($db);

echo json_encode($template->getTemplates());