<?php
// DB-Verbindung & Klassen importieren
include('../content/db.inc.php');
include('../classes/report.class.php');

// Klassen instanziieren
$report = new report($db);

if(!empty($_POST['id']))
{
	$report->updateReport($_POST['id'],$_POST['titel'],$_POST['year'],$_POST['description']);
}
else
{
	http_response_code(422);
    echo "Invalid parameters given!";
}