<?php
try{
	session_start();
	require('../content/db.inc.php');
	$master = array();
	
	//WAS FEHLT
	//bei move_uploaded_file, einen zusätzlichen ORderner mit userid erstellen + die files upload0-99
	
	$templateid = $_GET['templateid'];
	//$reportid = $_GET['reportid'];
	$allowimage = $_GET['allowimage'];
	$userid = $_SESSION['user_id'];
	
	//get reportid
		
	date_default_timezone_set("Europe/Berlin");
	$timestamp = time();
	
	$year = date("Y",$timestamp);
		
	$getreportid = $db->prepare("SELECT * from report where user_id=:curruser and year=:curryear");
	$getreportid->bindParam(':curruser', $userid);
	$getreportid->bindParam(':curryear', $year);
	$getreportid->execute();
	while($row = $getreportid->fetch(PDO::FETCH_ASSOC)){
		$reportid = $row['id'];
	}
	
	//get imgcount from template
	$getimgcount = $db->prepare("select * from template where id = :tempid");
	$getimgcount->bindParam(':tempid', $templateid);
	$getimgcount->execute();
	while($row = $getimgcount->fetch(PDO::FETCH_ASSOC)){
			$imgcount = $row['imgcount'];
	}
	//count images
	$countimgs = $db->prepare("SELECT count(*) FROM image WHERE report_id = :reportid");
	$countimgs->bindParam(':reportid', $reportid);
	$countimgs->execute();
	while($row = $countimgs->fetch(PDO::FETCH_ASSOC)){
			$countimg = $row['count(*)'];
	}
	
	$getusername = $db->prepare("SELECT username FROM user WHERE id=:userid");
	$getusername->bindParam(':userid', $userid);
	$getusername->execute();
	while($row = $getusername->fetch(PDO::FETCH_ASSOC)){
			$username = $row['username'];
	}
	
	$getyear = $db->prepare("SELECT * FROM report WHERE id=:reportid");
	$getyear->bindParam(':reportid', $reportid);
	$getyear->execute();
	while($row = $getyear->fetch(PDO::FETCH_ASSOC)){
			$year = $row['year'];
	}
	
	if($imgcount == $countimg)
	{
		throw new Exception("Sie können keine Bilder mehr hochladen", 1);
	}
	else
	{	
		if($allowimage == true)
		{
			$id = 0;
			$target_dir = "../uploads/".$username."/".$year."/";
			$uploadOk = 1;

			if(!file_exists($target_dir) && !is_dir($target_dir)){
				mkdir($target_dir,0777);
			}
			$fileid = 0;
			while(file_exists($target_dir .$reportid ."-".$fileid.".png")){
				$fileid ++;
			}
			$target_file = $target_dir .$reportid ."-".$fileid.".png";
			// Check file size
			if ($_FILES["file-0"]["size"] > 5000000) {
				throw new Exception("Sorry, your file is too big.", 1);
				$uploadOk = 0;
			}
			// Allow certain file formats
			/*if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
				throw new Exception("Sorry, only JPG, JPEG, PNG files are allowed.", 1);
				$uploadOk = 0;
			}*/
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				throw new Exception("Sorry, your file was not uploaded.", 1);	
			// if everything is ok, try to upload file
			} else {
				if (move_uploaded_file($_FILES["file-0"]["tmp_name"], $target_file)) {//anstatt $_Files upload0-99
					
					$imagedata = file_get_contents($target_file);         
					$base64 = base64_encode($imagedata);

					$saveimage =  $db->prepare('INSERT INTO image(code, report_id) VALUES(:code, :report_id)');
					$saveimage->bindParam(':code', $base64);
					$saveimage->bindParam(':report_id', $reportid);
					$saveimage->execute();
					
					$master[0] = 0;
					$master[1] = "File uploaded";
					echo json_encode($master);
					exit();
				} else {
					throw new Exception("Sorry, there was an error uploading your file.", 1);
				}
			}
		}
		else
			throw new Exception("Checkbox not checked");
	}
}
catch(Exception $e){
	$master[0] = $e->getCode();
	$master[1] = $e->getMessage();
	echo json_encode($master);
	exit();
}