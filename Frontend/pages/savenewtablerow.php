<?php
try{
	session_start();
	require('../content/db.inc.php');	
	
	$master = array();
	
	$newtablerowname = $_POST['newtablerowname'];
	$newtablerowval = $_POST['newtablerowval'];
	$reportid = $_POST['reportid'];
	
	$newtableentry = $db->prepare("INSERT INTO table_row(name, value, report_id) VALUES(:name, :val, :reportid)");
	$newtableentry->bindParam(':name', $newtablerowname, PDO::PARAM_STR);
	$newtableentry->bindParam(':val', $newtablerowval, PDO::PARAM_STR);
	$newtableentry->bindParam(':reportid', $reportid, PDO::PARAM_STR);
	
	if($newtableentry->execute()){
		$master[0] = 0;
		$master[1] = "Tablerow created";
		echo json_encode($master);
		exit();
	}
	else
		throw new Exception("Tablerow konnte nicht erzeugt werden", 1);
}
catch(Exception $e){
	$master[0] = $e->getCode();
	$master[1] = $e->getMessage();
	echo json_encode($master);
	exit();
}
?>