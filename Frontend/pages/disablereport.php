<?php
try{
	session_start();
	require('../content/db.inc.php');
	
	date_default_timezone_set("Europe/Berlin");
	$timestamp = time();
	
	$year = date("Y",$timestamp);
	
	$statusbool = 1;
	
	$getreportid = $db->prepare("SELECT id from report where user_id=:curruser and year=:curryear");
	$getreportid->bindParam(':curruser', $_SESSION['user_id']);
	$getreportid->bindParam(':curryear', $year);
	$getreportid->execute();
	while($row = $getreportid->fetch(PDO::FETCH_ASSOC)){
		$reportid = $row['id'];
    }
	
	echo $reportid;
	
	$updatestatus = $db->prepare('UPDATE status SET deleted=:del WHERE report_id=:reportid');
	$updatestatus->bindParam(':del', $statusbool);	
	$updatestatus->bindParam(':reportid', $reportid);
	
	$updatestatus->execute();
	
	header('Location: ../pages/index.php');
}
catch(Exception $e){
	die("SaveData Failed");
}