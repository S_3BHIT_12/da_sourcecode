<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<?php if (isset($_GET['errmsg']))
		{
			$err=$_GET['errmsg'];
			if($err == 1){?>
				<div class="alert alert-danger">
					<strong>Achtung!</strong> Sie haben bereits einen Bericht erstellen. Sie können keinen neuen erstellen
				</div>
		<?php 	}
			if($err == 2){?>
				<div class="alert alert-danger">
					<strong>Achtung!</strong> Ihr Bericht wurde akzeptiert. Sie können Ihren Bericht nicht mehr bearbeiten
				</div>
		<?php 	}
			if($err == 3){?>
				<div class="alert alert-danger">
					<strong>Achtung!</strong> Ihr Bericht wurde gelöscht. Sie können Ihren Bericht nicht mehr bearbeiten
				</div>
		<?php 	}
			if($err == 4){?>
				<div class="alert alert-danger">
					<strong>Achtung!</strong> Ihr Bericht wurde eingereicht. Sie können Ihren Bericht nicht mehr bearbeiten
				</div>
		<?php }	
			if($err == 5){?>
			<div class="alert alert-danger">
				<strong>Achtung!</strong> Sie haben noch keinen Bericht erstellt. Bitte erstellen Sie einen um diesen bearbeiten zu können
			</div>
			<?php } 
		}?>
<!-- Gelbe Farbe für Website #ddaf00 -->
<!-- 2. Gelbe Farbe für Website #f9d74d -->
<!-- 3. Gelbe Farbe für Website #ffd800 -->
<!-- Blaue Farbe für Website #002b6d -->
<main>
		<div style="padding-top: 3%" class="sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul style="width: 200px;" class="nav" id="side-menu">
                    <li style="margin-bottom: 1.5%;">
                        <a style="background-color:#FFE864;" class="btn btn-default" href="jahresbericht.php" role="button">
							<p style="margin-bottom: 0px; display:inline;">Dokument erstellen</p>
						</a>
                    </li>
                    <li>
                        <a style="background-color:#FFE864;" class="btn btn-default" href="existingreport.php" role="button">
							<p style="margin-bottom: 0px; display:inline;">Dokument bearbeiten</p>
						</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
	<div>	
	<div style="margin: 2%" id="reportstatus">
	</div>
</main>
<script>
	//Check Report Status
	$(document).ready(function(){
		$.ajax({
			url: "../apis/getStatus.api.php",
			type: "POST",
			success:function(data) {
                var reportstatus = JSON.parse(data);
				console.log(reportstatus);
				if(reportstatus[0]['accepted'] == 1)
					$('#reportstatus').append("<h2>Berichtstatus: Ihr Jahresbericht "+reportstatus[0]['year']+" ist akzeptiert worden!</h2>");
				else if(reportstatus[0]['deleted'] == 1)					
					$('#reportstatus').append("<h2>Berichtstatus: Ihr Bericht "+reportstatus[0]['year']+" ist gelöscht worden!</h2>");
				else if(reportstatus[0]['submitted'] == 1)
					$('#reportstatus').append("<h2>Berichtstatus: Ihr Bericht "+reportstatus[0]['year']+" ist eingereicht worden!</h2>");
				else if(reportstatus[0]['declined'] == 1)
					$('#reportstatus').append("<h2>Berichtstatus: Ihr Bericht "+reportstatus[0]['year']+" ist abgelehnt worden!</h2>");
			},
			 error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
	});
	
</script>

<?php include("../content/footer.inc.php"); ?>