<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<main style="height:100%">
	<div style="height: 10px;"></div>
	<div id="errmsg"></div>
	<!-- Formular Section -->
	<div id="eingabetext" style="width: 30%; float: left; margin-left:2%;"><!--class="leftSection-->
			<!--<form method="POST" id="updatejahresbericht"> <!-- action="../pages/updateData.php -->
				<!-- hier content aus table row -->

				<input type="hidden" class="form-control" id="templateid" name="templateid">
				<input type="hidden" class="form-control" id="reportid" name="reportid">
				<div class="row" style="margin-bottom:10px;">
					<label style="display: block; padding-left:2.5%">Template</label>
					<div class="col-lg-6">
						<select class="form-control" id="templatecnt">
							
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label>Hier können sie Ihre Tabelleneinträge bearbeiten</label>
					<button class="btn btn-primary btn-default dropdown-toggle" type="button" id="dropdownMenuButton_fahrer_loeschen" data-toggle="modal" data-target="#tablerow_update_popup">Tabelleneinträge bearbeiten</button>
				</div>
				<div class="form-group">
					<label for="titel">Überschrift</label>
					<input type="text" class="form-control" id="titel" name="titel" placeholder="Überschrift">
				</div>
				<div class="form-group">
					<label for="description">Bericht</label>
					<textarea class="form-control" rows="7" id="description" name="description" placeholder="Bericht"></textarea>
				</div>
				<input type="hidden" class="form-control" id="templateid2" name="templateid2">
				<input type="hidden" class="form-control" id="reportid2" name="reportid2">
				<input type="hidden" class="form-control" id="templateid3" name="templateid3">
				<div class="form-group">
					<label style="display: block;">Hier können sie Ihre Bilder löschen!</label>
					<button class="btn btn-primary btn-default dropdown-toggle" type="button" id="dropdownMenuButton_imagelöschen" data-toggle="modal" data-target="#delete_image">Bilder löschen</button>
				</div>
				<input type="checkbox" name="allowimage" id="allowimage" checked>
					<label for="allowimage">Ich erkläre, über sämtliche Verwertungsrechte an den übermittelten Fotos zu verfügen. Sämtliche abgebildeten Personen haben ihr Einverständnis zur Veröffentlichung im Rahmen der Website von NÖGUS - Initiative »Tut gut!« erteilt.</label>
				
				<label for="uploadpictures">Bilder hochladen</label>
				<input type="file" name="fileToUpload" id="fileToUpload">
				<button id="upload">Upload Image</button>
				<p class="help-block">Hier können sie Ihre Bilder hochladen.</p>
				
				<button class="btn btn-default" id="save">Speichern</button>
				
				
				<!-- PO UP -->
				<div class="modal fade" id="tablerow_update_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<table class="table table-striped table-bordered table-hover">
											<tbody id="tablerowupdate">
												
											</tbody>
										</table>
															
									</div>
									<div>
										<label>Hier können sie weitere Einträge in die Tabelle einfügen</label>
											<table class="table table-striped table-bordered table-hover">
												<tbody id="newentries">
													<tr>					
														<tr id="newrows">
															<td><input id="newentryname" class="form-control" name="newentryname"></td>
															<td><input id="newentryval" class="form-control" name="newentryval"></td>
															<td><button id="newentry" class="btn btn-default form-control">Hinzufügen</button></td>
														</tr>
													</tr>	
												</tbody>
											</table>
									</div>		
								</div>
							</div>
					</div>
					<div class="modal fade" id="delete_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<table class="table table-striped table-bordered table-hover">
											<tbody id="deleteimages">
												
											</tbody>
										</table>
															
									</div>
								</div>
							</div>
					</div>
			<!--</form>-->
			<button class="btn btn-default" id="submitReport">Abgeben</button>
			<button class="btn btn-default" id="deletereport">Verwerfen</button>
	</div>
	
	<!-- Live-Vorschau Section-->
	<div style="border: 1px solid #66afe9; border-radius: 4px; height:29.7cm; width:21cm; transform: scale(0.7); transform-origin: left top; background-color:white; float:right;" id="showresult">
		<!-- Live Vorschau -->
	</div>				
	<script>
	//Bei der selectbox immmer das verwendete TEmplate auswählen
	
	$(document).ready(function(){
		$.ajax({
			url: "../apis/checkifreportexits.api.php",
			type: "POST",
			success:function(data) {
                //var check = JSON.parse(data);
				console.log(data);
				if(data)
					window.location.href = "../pages/index.php?errmsg=5";
			},
			 error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
		$.ajax({
			url: "../apis/getReportStatus.api.php",
			type: "POST",
			success:function(data) {
                var reportstatus = JSON.parse(data);
				if(reportstatus[0]['accepted'] == 1)
					window.location.href = "../pages/index.php?errmsg=2";
				else if(reportstatus[0]['deleted'] == 1)					
					window.location.href = "../pages/index.php?errmsg=3";
				else if(reportstatus[0]['submitted'] == 1)
					window.location.href = "../pages/index.php?errmsg=4";
			},
			 error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
		$.ajax({
            url:"../apis/getTemplates.api.php",
            success:function(data) {
				console.log('getTemplates works!');
                var getTemplates = JSON.parse(data);
				console.log(getTemplates.length);
				console.log(data);
				for(i=0; i < getTemplates.length; i++){				
					$('#templatecnt').append('<option data-id="'+getTemplates[i]['id']+'">'+getTemplates[i]['id']+'</option>');
				}
				//function get report
				getreport();
				gettablerows();
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
		
		
		$.ajax({
			url: "../apis/getImages.api.php",
			type: "POST",
			success:function(data) {
				var imagelist = JSON.parse(data);
				for(i = 0; i < imagelist.length; i++) {
					$('#deleteimages').append('<tr><td>Bild '+imagelist[i]['id']+'</td><td><a href="../apis/deleteimage.api.php?id='+imagelist[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Löschen</a></td></tr>');
				}
			},
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
	});

	$('#titel, #description').keyup(function(){
		fillTemplate();
	});
	
	$('#templatecnt').change(function(){
		fillTemplate();
	});
	$('#tablerowupdate').change(function(){
		fillTemplate();
	});
	function gettablerows(){
		$('#tablerowupdate').empty();
		$.ajax({
			url: "../apis/getTableRows.api.php",
			type: "POST",
			success:function(data) {
				console.log(data);
				var attributeList = JSON.parse(data);
				console.log(attributeList);
				for(i = 0; i < attributeList.length; i++) {
					$('#tablerowupdate').append('<tr><td><input type="hidden" id="tablerowname" name="tablerowname" class="form-control" value="'+attributeList[i]['name']+'"><label class="form-control">'+attributeList[i]['name']+'</label></td><td><input id="tablerowval" class="form-control" value="'+attributeList[i]['value']+'"></td><td><a href="../apis/deletetablerow.api.php?id='+attributeList[i]['id']+'"><p class="glyphicon glyphicon-remove"></p>Löschen</a></td></tr>');
				}
			},
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
	}
	function getreport(){
		$.ajax({
			url: "../apis/getReport.api.php",
			type: "POST",
			success:function(data) {
                var report = JSON.parse(data);
				console.log(report);
				$('#templateid').val(report['template_id']);
				$('#templatecnt').val(report['template_id']);
				$('#reportid').val(report['id']);
				$('#reportid2').val(report['id']);
				$('#titel').val(report['heading']);
				$('#description').val(report['content']);
				fillTemplate();
            },
			 error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
	}
//FILL THAT TEMPLATE ON KEY UP - DOCUMENT READY
	function fillTemplate(){
		var templateid = $('#templatecnt option:selected').attr("data-id");
		$.ajax({
			url: "../apis/fillTemplateUpdate.api.php",
			type: "POST",
			data: {	templateid: templateid,
					heading: $('#titel').val(),
					content: $('#description').val()		
			},
			success:function(data) {
				console.log(data);
				$('#showresult').empty();
				$('#showresult').append(data);
			},
			error:function(data,textStatus,errorThrown) {
				alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
			}
		});
	}
	$('#upload').click(function(){
		var formdata = new FormData();
		$.each($('#fileToUpload')[0].files, function(i, file){
			formdata.append('file-' + i, file);
		});
		var templateid = $('#templatecnt option:selected').attr("data-id");
		var reportid = $('#reportid').val();
		var allowimage = $('#allowimage').val();
		$.ajax({
			url: "../pages/uppic.php?templateid="+ templateid + "&reportid="+ reportid +"&allowimage="+ allowimage, //+ reportid + templateid + allowimage
			type: "POST",
			data: formdata,
			contentType: false,
			processData: false,
			success: function(data){
				if(data[0] == 0){
					//alert(data[0]);
					console.log(data[0]);
					fillTemplate();
					getreport();
				}	
				else{
					//alert(data[1]);
					console.log(data[1]);
					fillTemplate();
					getreport();
				}
			},
			error:function(data,textStatus,errorThrown) {
				//alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				alert(data[0]);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
			}
		});
	});
	
	$('#updatetablerow').click(function(){
		var reportid = $('#reportid').val();
		alert(reportid);
		$.ajax({
			url:"../apis/deleterows.api.php",
			type: 'POST',
			dataType: 'json',
			data:{
				reportid: reportid
			},
			success:function() {
				$('#tablerowupdate').children().each(function(){
					var rowname = $(this).find('#tablerowname').val();
					var rowval = $(this).find('#tablerowval').val();
					if(rowname != undefined && rowval != undefined)
					{	
						alert(rowname + ' ' + rowval);
						$.ajax({
							url: '../apis/inserttablerow.api.php',
							type: 'POST',
							dataType: 'json',
							data:{
								rowname: rowname,
								rowval: rowval,
								reportid: reportid
							},
							success: function(data){
								console.log("It works");
								consolel.log(data);
							},
							error:function(data,textStatus,errorThrown) {
								alert(textStatus+"\n"+errorThrown+"\n"+data.status);
								console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
								console.log(data);
							}
						});
					}
					else
						alert("Failes");
				});
			},
			error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				alert("Jetzt versteh i gar nix mehr")
				console.log(data);
            }
		});		
	});
	
	$('#newentry').click(function(){
		var newtablerowname = $('#newentryname').val();
		var newtablerowval = $('#newentryval').val();
		var reportid = $('#reportid').val();
		$.ajax({
			url: '../pages/savenewtablerow.php',
			type: 'POST',
			dataType: 'json',
			data: {
				newtablerowname: newtablerowname,
				newtablerowval: newtablerowval,
				reportid: reportid
			},
			success: function(data){
				if(data[0] == 0){
					//alert(data[0]);
					gettablerows();
					fillTemplate();
					console.log(data[0]);					
				}	
				else{
					//alert(data[1]);
					//fehlgeschlagen
					console.log(data[1]);
				}
			},
			error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
	});
	$('#save').click(function(){
		var tempid = $('#templatecnt option:selected').attr("data-id");
		var heading = $('#titel').val();
		var content = $('#description').val();
		$.ajax({
			url: '../pages/updateData.php',
			type: 'POST',
			dataType: 'json',
			data: {
				tempid: tempid,
				heading: heading,
				content: content
			},
			success: function(data){
				console.log(data);
				window.location.href = "../pages/index.php";
			},
			error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
	});
	$('#submitReport').click(function(){
		$.ajax({
			url: "../pages/abgeben.php",
			success:function(data) {
				console.log('It works');
				window.location.href = "../pages/index.php";
            },
			 error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
	});
	$('#deletereport').click(function(){
		window.location.href = "../pages/index.php";
	});
	</script>
</main>
<?php include("../content/footer.inc.php"); ?>