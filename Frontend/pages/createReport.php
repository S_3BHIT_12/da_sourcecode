<?php

try{
	session_start();
	require('../content/db.inc.php');
	$master = array();
	
	//echo $_SESSION['user_id'];
	
	//Titel
	$heading = htmlspecialchars($_POST['heading']);
	//echo $title;
	
	//Derzeitiges Jahr
	date_default_timezone_set("Europe/Berlin");
	$timestamp = time();
	
	$year = date("Y",$timestamp);
	//echo $year;
	
	//Bericht
	$content = $_POST['content'];
	//echo $description;
	
	//Template_ID
	$template_id = $_POST['templateid'];
	//echo $template_id;
	
	$curruser = $_SESSION['user_id'];
	$statusbool = 0;
	
	//report erstellen/updaten da er ja schon existiert
	
	$getreport = $db->prepare("SELECT id from report where user_id=:curruser and year=:curryear");
	$getreport->bindParam(':curruser', $curruser);
	$getreport->bindParam(':curryear', $year);
	$getreport->execute();
	while($row = $getreport->fetch(PDO::FETCH_ASSOC)){
		$reportid = $row['id'];
    }
	
	/*$getimgcount = $db->prepare("SELECT imgcount FROM template WHERE id=:tempid");
	$getimgcount->bindParam(':tempid', $template_id);
	$getimgcount->execute();
	while($row = $getimgcount->fetch(PDO::FETCH_ASSOC)){
		$imgcount = $row['imgcount'];
    }
	
	$countimgs = $db->prepare("SELECT count(*) FROM image WHERE report_id = :reportid");
	$countimgs->bindParam(':reportid', $reportid);
	$countimgs->execute();
	while($row = $countimgs->fetch(PDO::FETCH_ASSOC)){
			$countimg = $row['count(*)'];
	}*/
	
		$savedata = $db->prepare("UPDATE report SET heading=:heading, content=:content, template_id=:templateid WHERE id=:reportid");
		$savedata->bindParam(':heading', $heading, PDO::PARAM_STR);
		$savedata->bindParam(':content', $content, PDO::PARAM_STR);
		$savedata->bindParam(':templateid', $template_id, PDO::PARAM_STR);
		$savedata->bindParam(':reportid', $reportid, PDO::PARAM_STR);
		$savedata->execute();
		
		//table_row: name
		$gname = $_POST['gname'];
		$plz = $_POST['plz'];
		$bev = $_POST['bev'];
		
		//table_row: value
		$gval = $_POST['gval'];
		$plzval = $_POST['plzval'];
		$bevval = $_POST['bevval'];
		
		$createTableRow1 =$db->prepare("INSERT INTO table_row(name, value, report_id) VALUES(:name, :val, :reportid)");
		$createTableRow1->bindParam(':name', $gname, PDO::PARAM_STR);
		$createTableRow1->bindParam(':val', $gval, PDO::PARAM_STR);
		$createTableRow1->bindParam(':reportid', $reportid, PDO::PARAM_STR);
		$createTableRow1->execute();
		
		$createTableRow2 =$db->prepare("INSERT INTO table_row(name, value, report_id) VALUES(:name, :val, :reportid)");
		$createTableRow2->bindParam(':name', $plz, PDO::PARAM_STR);
		$createTableRow2->bindParam(':val', $plzval, PDO::PARAM_STR);
		$createTableRow2->bindParam(':reportid', $reportid, PDO::PARAM_STR);
		$createTableRow2->execute();
		
		$createTableRow3 =$db->prepare("INSERT INTO table_row(name, value, report_id) VALUES(:name, :val, :reportid)");
		$createTableRow3->bindParam(':name', $bev, PDO::PARAM_STR);
		$createTableRow3->bindParam(':val', $bevval, PDO::PARAM_STR);
		$createTableRow3->bindParam(':reportid', $reportid, PDO::PARAM_STR);
		$createTableRow3->execute();
		
		$master[0] = 0;
		$master[1] = "Bericht erstellt";
		echo json_encode($master);
		exit();
}
catch(Exception $e){
	$master[0] = $e->getCode();
	$master[1] = $e->getMessage();
	echo json_encode($master);
	exit();
}