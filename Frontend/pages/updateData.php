<?php
try{
	session_start();
	require('../content/db.inc.php');
	$master = array();
	
	//Titel
	$titel = $_POST['heading'];
	
	date_default_timezone_set("Europe/Berlin");
	$timestamp = time();
	
	$year = date("Y",$timestamp);
	
	//Content
	$description = $_POST['content'];
	
	$tempid = $_POST['tempid'];
	
	$updatereport = $db->prepare('UPDATE report SET heading=:heading, content=:description, template_id=:tempid WHERE year=:year AND user_id=:curruser');
	$updatereport->bindParam(':heading', $titel);	
	$updatereport->bindParam(':description', $description);
	$updatereport->bindParam(':year', $year);
	$updatereport->bindParam(':tempid', $tempid);
	$updatereport->bindParam(':curruser', $_SESSION['user_id']);
	
	$updatereport->execute();
	
	$master[0] = 0;
	$master[1] = "Bericht gespeichert";
	echo json_encode($master);
	exit();
}
catch(Exception $e){
	$master[0] = $e->getCode();
	$master[1] = $e->getMessage();
	echo json_encode($master);
	exit();
}