<?php
try{
	session_start();
	require('../content/db.inc.php');
	
	date_default_timezone_set("Europe/Berlin");
	$timestamp = time();
	
	$year = date("Y",$timestamp);
	
	$statusbool = 1;
	
	$getreportid = $db->prepare("SELECT id from report where user_id=:curruser and year=:curryear");
	$getreportid->bindParam(':curruser', $_SESSION['user_id']);
	$getreportid->bindParam(':curryear', $year);
	$getreportid->execute();
	while($row = $getreportid->fetch(PDO::FETCH_ASSOC)){
		$reportid = $row['id'];
    }
	
	echo $reportid;
	
	$updatestatus = $db->prepare('UPDATE status SET submitted=:sub WHERE report_id=:reportid');
	$updatestatus->bindParam(':sub', $statusbool);	
	$updatestatus->bindParam(':reportid', $reportid);
	
	$updatestatus->execute();
	
	//Email versenden
	
	$currentUser = $db->prepare('select username, email from user where id=:currentUser');
	$currentUser->bindParam(':currentUser', $_SESSION['user_id'], PDO::PARAM_STR);
	$currentUser->execute();
	while($row = $currentUser->fetch(PDO::FETCH_ASSOC)){
		$user = $row['username'];
		$email = $row['email'];
    }
	
	$empfaenger = "pfeffer.dani98@gmail.com";
	$betreff = "Bericht eingereicht";
	$from = "From: ".$user." <".$email.">";
	$text = "Der Bericht der Gemeinde ".$user." ist eingereicht worden.";
	 
	mail($empfaenger, $betreff, $text, $from);
	
	header('Location: ../pages/index.php');
}
catch(Exception $e){
	die("SaveData Failed");
}