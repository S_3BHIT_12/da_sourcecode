﻿<?php include("../content/head.inc.php"); ?>
<?php include("../content/nav.inc.php"); ?>
<main>
	<div style="height: 10px;"></div>
	<div id="errmsg"></div>
	<!-- Formular Section -->
	<div id="eingabetext" style="width: 30%; float: left; margin-left:2%" class="leftSection">
	
			<!--<form method="POST" id="jahresbericht"> <!--action="../pages/createReport.php" -->			
				<!-- hier content aus table row -->
				<div class="row" style="margin-bottom:10px;">
					<label>Template</label>
					<div class="col-lg-6">
						<select class="form-control" id="templatecnt">
							<!-- Verfügbare Templates -->
						</select>
					</div>
				</div>
				<input type="hidden" class="form-control" id="reportid" name="reportid"/>
				<label>Gemeindedaten</label>
				<table class="table table-striped table-bordered table-hover">
					<tbody id="tablebody"> <!-- Content of Table (document ready)-->
						<tr>					
							<tr id="gemeindenamecont">
								<td><input type="hidden" id="gemeindename" class="form-control" name="gemeindename" value="Gemeindename"/><label class="form-control">Gemeindename</label></td>
								<td><input id="gemeindenamevalue" class="form-control" name="gemeindenamevalue"/></td>
							</tr>
							<tr>
								<td><input type="hidden" id="plz" class="form-control" name="plz" value="PLZ"/><label>PLZ</label></td>
								<td><input id="plzvalue" class="form-control" name="plzvalue"/></td>	
							</tr>
							<tr>
								<td><input type="hidden" id="bevoelkerung" class="form-control" name="bevoelkerung" value="Bevölkerung"/><label class="form-control">Bevölkerung</label></td>
								<td><input id="bevoelkerungvalue" class="form-control" name="bevoelkerungvalue"/></td>
							</tr>
						</tr>	
					</tbody>
				</table>
				<div class="form-group">
					<label for="titel">Überschrift</label>
					<input type="text" class="form-control" id="titel" name="titel" placeholder="Überschrift"/>
				</div>
				
				<div class="form-group">
					<label for="description">Bericht</label>
					<textarea class="form-control" rows="7" id="description" name="description" placeholder="Bericht"></textarea>
				</div>	
					<input type="checkbox" name="allowimage" id="allowimage"/>
					<label for="allowimage">Ich erkläre, über sämtliche Verwertungsrechte an den übermittelten Fotos zu verfügen. Sämtliche abgebildeten Personen haben ihr Einverständnis zur Veröffentlichung im Rahmen der Website von NÖGUS - Initiative »Tut gut!« erteilt.</label>
					
					<label for="uploadpictures">Bilder hochladen</label>
					<input type="file" name="fileToUpload" id="fileToUpload"/>
					<button id="upload">Upload Image</button>
					<p class="help-block">Hier können sie Ihre Bilder hochladen.</p>
					<button class="btn btn-default" id="create">Erstellen</button>
					<button class="btn btn-default" id="deletereport">Verwerfen</button>

			<!--</form>-->					
	</div>
	
	<!-- Live-Vorschau Section-->
	<div style="border: 1px solid #66afe9; border-radius: 4px; height:29.7cm; width:21cm; transform: scale(0.7); transform-origin: left top; background-color:white; float:right;" id="showresult">
		<!-- Live Vorschau -->
	</div>
	
	<div class="modal fade" id="delete_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
							<table class="table table-striped table-bordered table-hover">
								<tbody id="deleteimages">
									
								</tbody>
							</table>
												
                        </div>
					</div>
				</div>
		</div>			
</main>
<script>
	$(document).ready(function(){
        $.ajax({
			url: "../apis/checkifreportalreadyexits.api.php",
			type: "POST",
			success:function(data) {
                //var check = JSON.parse(data);
				console.log(data);
				if(data){
					window.location.href = "../pages/index.php?errmsg=1";
				}
				else
				{
					$.ajax({
						url:"../apis/createReport.api.php",
						success:function(data) {
							console.log("report created");
						},
						error:function(data,textStatus,errorThrown) {
							alert(textStatus+"\n"+errorThrown+"\n"+data.status);
						}
					});
				}
			},
			 error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
		$.ajax({
            url:"../apis/getTemplates.api.php",
            success:function(data) {
				console.log('getTemplates works!');
                var getTemplates = JSON.parse(data);
				console.log(getTemplates.length);
				console.log(data);
				for(i=0; i < getTemplates.length; i++){				
					$('#templatecnt').append('<option data-id="'+getTemplates[i]['id']+'">Template '+getTemplates[i]['id']+'</option>');
				}
				
            },
            error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
            }
        });
		//report id auslesen
	});
	$('#titel, #description').keyup(function(){
		fillTemplate();
	});
	
	$('#templatecnt').change(function(){
		fillTemplate();
	});
	$('#tablebody').keyup(function(){
		fillTemplate();
	});
//FILL THAT TEMPLATE ON KEY UP - DOCUMENT READY
	function fillTemplate(){
		
		var gname = $('#gemeindename').val();
		var plz = $('#plz').val();
		var bev = $('#bevoelkerung').val();
		
		var gval = $('#gemeindenamevalue').val();
		var plzval = $('#plzvalue').val();
		var bevval = $('#bevoelkerungvalue').val();
		
		$.ajax({
			url: "../apis/fillTemplateCreate.api.php",
			type: "POST",
			data: {
					gname: gname,
					plz: plz,
					bev: bev,
					gval: gval,
					plzval: plzval,
					bevval: bevval,
					heading: $('#titel').val(),
					content: $('#description').val(),
					templateid: $('#templatecnt option:selected').attr("data-id")
			},
			success:function(data) {
				console.log(data);
				$('#showresult').empty();
				$('#showresult').append(data);
			},
			error:function(data,textStatus,errorThrown) {
				alert(textStatus+"\n"+errorThrown+"\n"+data.status);
			}
		});
	}
	$('#upload').click(function(){
		var formdata = new FormData();
		$.each($('#fileToUpload')[0].files, function(i, file){
			formdata.append('file-' + i, file);
		});
		var templateid = $('#templatecnt option:selected').attr("data-id");
		//var reportid = $('#reportid').val();
		var allowimage = $('#allowimage').val();
		//alert(formdata + allowimage + templateid);
		$.ajax({
			url: "../pages/uploadpictures.php?templateid="+ templateid +"&allowimage="+ allowimage, //+ reportid + templateid + allowimage
			type: "POST",
			data: formdata,
			contentType: false,
			processData: false,
			success: function(data){
				if(data[0] == 0){
					fillTemplate();
					console.log(data[0]);
				}	
				else{
					fillTemplate();
					console.log(data[1]);
				}
			},
			error:function(data,textStatus,errorThrown) {
				alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				alert(data[0]);
				console.log(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
			}
		});
	});
	$('#create').click(function(){	
		//table_row: name
		var gname = $('#gemeindename').val();
		var plz = $('#plz').val();
		var bev = $('#bevoelkerung').val();
		
		//alert(gname + ' ' + plz + ' ' + bev);
		
		//table_row: value
		var gval = $('#gemeindenamevalue').val();
		var plzval = $('#plzvalue').val();
		var bevval = $('#bevoelkerungvalue').val();
		
		//alert(gval + ' ' + plzval + ' '+ bevval);
		
		var templateid = $('#templatecnt option:selected').attr("data-id");
		
		var heading = $('#titel').val();
		var content = $('#description').val();
		
		//alert(templateid + ' ' + heading + ' ' + content);
		
		$.ajax({
			url: '../pages/createReport.php',
			type: 'POST',
			dataType: 'json',
			data: {
				gname: gname,
				plz: plz,
				bev: bev,
				gval: gval,
				plzval: plzval,
				bevval: bevval,
				templateid: templateid,
				heading: heading,
				content: content
			},
			success: function(data){
				window.location.href = "../pages/index.php";
				console.log(data);
			},
			error:function(data,textStatus,errorThrown) {
                alert(textStatus+"\n"+errorThrown+"\n"+data.status);
				console.log(data);
            }
		});
	});
	$('#deletereport').click(function(){
		window.location.href = "../pages/index.php";
	});
</script>
<?php include("../content/footer.inc.php"); ?>