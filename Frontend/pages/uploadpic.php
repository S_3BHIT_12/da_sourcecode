<?php
try{
	session_start();
	require('../content/db.inc.php');
	$master = array();
	
	
	$templateid = $_GET['templateid'];
	$allowimage = $_GET['allowimage'];
	
	/*$getreport = $db->prepare("SELECT id from report where user_id=:curruser and year=:curryear");
	$getreport->bindParam(':curruser', $_SESSION['user_id']);
	$getreport->bindParam(':curryear', $year);
	$getreport->execute();
	while($row = $getreport->fetch(PDO::FETCH_ASSOC)){
		$reportid = $row['id'];
    }*/
	
	//get imgcount from template
	$getimgcount = $db->prepare("select * from template where id = :tempid");
	$getimgcount->bindParam(':tempid', $templateid);
	$getimgcount->execute();
	while($row = $getimgcount->fetch(PDO::FETCH_ASSOC)){
			$imgcount = $row['imgcount'];
	}
	//count images
	$countimgs = $db->prepare("SELECT count(*) FROM image WHERE report_id = :reportid");
	$countimgs->bindParam(':reportid', $reportid);
	$countimgs->execute();
	while($row = $countimgs->fetch(PDO::FETCH_ASSOC)){
		$countimg = $row['count(*)'];
	}	
	
	if($imgcount == $countimg)
	{
		throw new Exception("Sie können keine Bilder mehr hochladen", 1);
	}
	else
	{	
		if($allowimage == true)
		{
		
			$target_dir = "../uploads/";
			$target_file = $target_dir . basename($_FILES["file-0"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

			// Check file size
			if ($_FILES["file-0"]["size"] > 5000000) {
				echo "Sorry"; //throw new Exception("Sorry, your file is too big.", 1);
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
				echo "Sorry"; //throw new Exception("Sorry, only JPG, JPEG, PNG files are allowed.", 1);
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				echo "Sorry";//throw new Exception("Sorry, your file was not uploaded.", 1);	
			// if everything is ok, try to upload file
			} else {
				if (move_uploaded_file($_FILES["file-0"]["tmp_name"], $target_file)) {
					$imagedata = file_get_contents($target_file);         
					$base64 = base64_encode($imagedata);

					//$saveimage =  $db->prepare('INSERT INTO image(code, report_id) VALUES(:code, :report_id)');
					//$saveimage->bindParam(':code', $base64);
					//$saveimage->bindParam(':report_id', $reportid);
					//$saveimage->execute();
					
					//$master[0] = 0;
					//$master[1] = $base64;
					echo $base64;
					exit();
				} else {
					//throw new Exception("Sorry, there was an error uploading your file.", 1);
					echo "Sorry";
				}
			}
		}
		else
			echo "Checkbox not checked";//throw new Exception("Checkbox not checked");
	}
}
catch(Exception $e){
	/*$master[0] = $e->getCode();
	$master[1] = $e->getMessage();
	echo json_encode($master);
	exit();*/
	die("SaveData Failed");
}