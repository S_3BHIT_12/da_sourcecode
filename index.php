<?php
	session_start();
	
	if (!(isset($_SESSION['user_id'])))
	{
		if (!(isset($_SESSION['admin_id'])))
		{
			header("location: login.php");
		}
	}
	
	if (isset($_SESSION['admin_id']))
	{
		header("location: /da_sourcecode/Backend/index.html");
	}
	
	if (isset($_SESSION['user_id']))
	{
		header("location: /da_sourcecode/Frontend/index.html");
	}
?>